# XÂY DỰNG WEBSITE THƯƠNG MẠI ĐIỆN TỬ
## GIỚI THIỆU

* Xây dựng một  progressive web app dạng single page cho phép khách hàng có thể xem, tìm kiếm sản phẩm và mua sản phẩm ngay trên website, ngoài ra khách hàng còn có thể xem các tin tức của cửa hàng, so sánh các sản phẩm cùng loại, xem lại lịch sử đơn hàng, danh sách sản phẩm yêu thích. Đồng thời hệ thống còn cho phép quản trị viên quản lý danh mục sản phẩm, sản phẩm, tin tức, đơn hàng, xem thống kê.
* Đối tượng người sử dụng bao gồm: khách truy cập, khách hàng là thành viên, nhân viên của hệ thống.
* Hệ thống gồm 2 module: Front-End và Back-End.

## MODULE FRONT-END

### Module này gồm các chức năng sau:
*	Menu trang web.
*	Chức năng tìm kiếm sản phẩm.
*	Hiển thị danh mục sản phẩm.
*	Banner quảng cáo.
*	Hiển thị danh sách sản phẩm theo loại sản phẩm. Cho phép người dùng sắp xếp theo tên, giá, lượt xem, số lượng bán.
*	Hiển thị thông tin chi tiết của một sản phẩm, cho phép xem các chi nhánh còn hàng của sản phẩm đó, xem các sản phẩm liên quan.
*	Giỏ hàng, đặt hàng.
*	Hiển thị các sản phẩm bán chạy, sản phẩm mới, sản phẩm giảm giá.
*	Xem danh sách tin tức và chi tiết của tin tức đó.
*	Trang thông tin khách hàng đã đăng ký: cho phép khách hàng cập nhật thông tin, xem lại lịch sử đơn hàng.
*	So sánh sản phẩm.
*	Đăng nhập, đăng ký, lấy lại mật khẩu của khách hàng.

## MODULE BACK-END
### Module này gồm các chức năng:
*	Đăng nhập/ lấy lại mật khẩu đã quên.
*	Quản lý nhân viên.
*	Quản lý khách hàng
*	Quản lý phân quyền nhân viên.
*	Quản lý danh mục sản phẩm.
*	Quản lý sản phẩm, thuộc tính của sản phầm.
*	Quản lý thương hiệu.
*	Quản lý đơn hàng.
*	Quản lý chi nhánh.
*	Quản lý menu website.
*	Quản lý địa điểm.
*	Quản lý banner.
*	Quản lý thư viện hình ảnh.
*	Quản lý tin tức.

## GIAI ĐOẠN PHÁT TRIỂN
### Giai đoạn 1
-	Thu thập và phân tích yêu cầu
-	Thiết kế hệ thống
### Giai đoạn 2
-	Chia các task
-	Thực hiện các task
### Giai đoạn 3
-	Test
-	Triển khai dự án lên server