/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js");

importScripts(
  "/precache-manifest.24fd5c67a81dc8bd265334e04dd05808.js"
);

importScripts('/js/idb.js');
importScripts('/js/utility.js');

workbox.clientsClaim();

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerNavigationRoute("/index.html", {
  
  blacklist: [/^\/_/,/\/[^\/]+\.[^\/]+$/],
});

workbox.routing.registerRoute(
  new RegExp('/css/'),
  workbox.strategies.networkFirst()
);
workbox.routing.registerRoute(
  new RegExp('/images/'),
  workbox.strategies.networkFirst()
);
workbox.routing.registerRoute(
  new RegExp('/js/'),
  workbox.strategies.networkFirst()
);

self.addEventListener('sync', function (event) {
  console.log('[Service Worker] Background syncing', event);
  if (event.tag === 'sync-new-checkout') {
    console.log('[Service Worker] Syncing new checkout');
    event.waitUntil(

      readAllData('sync-checkout')
        .then(function (data) {
          for (var dt of data) {
            console.log(dt.data);
            let dataSend = {};
            for(let key in dt.data) {
              dataSend[key] = dt.data[key];
            }

            fetch('https://e2018.aseanskills2018.com/api/v1/place-order', {
            // fetch('http://ecommerce2018.local/api/v1/place-order', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify(dataSend)
            }).then(function (res) {
              console.log('Sent data', res);
              if (res.ok) {
                self.registration.showNotification('Trạng thái đặt hàng', {
                  body: 'Đơn hàng của bạn đã được đặt thành công',
                  icon: '/icons/android-icon-96x96.png',
                  badge: '/icons/android-icon-96x96.png'
                })
                res.json().then(function (resData) {
                  deleteItemFromData('sync-checkout', dt.id);
                });
              }
            }).catch(function (err) {
              console.log('Error while sending data', err);
            });
          }

        })
    );
  }
});

self.addEventListener('push', function(event) {
  console.log('Push Notification received', event);

  var dataResponse = {title: 'E-Commerce 2018 - Thông báo!', body: 'Something new happened!', openUrl: '/'};

  if (event.data) {
    dataResponse = JSON.parse(event.data.text());
  }
console.log('[DATA PUSH]', dataResponse);
  var options = {
    body: dataResponse.body,
    icon: '/icons/android-icon-96x96.png',
    badge: '/icons/android-icon-96x96.png',
    image: dataResponse.data.image,
    data: {
      url: dataResponse.data.openUrl
    }
  };

  event.waitUntil(
    self.registration.showNotification(dataResponse.title, options)
  );
});

self.addEventListener('notificationclick', function(event) {  
  console.log('On notification click: ', event.notification.tag);  
  // Android doesn't close the notification when you click on it  
  // See: http://crbug.com/463146  
  var notification = event.notification;
  event.notification.close();
  // This looks to see if the current is already open and  
  // focuses if it is  
  event.waitUntil(
    clients.matchAll()
      .then(function(clis) {
        var client = clis.find(function(c) {
          return c.visibilityState === 'visible';
        });

        if (client !== undefined) {
          client.navigate(notification.data.url);
          client.focus();
        } else {
          clients.openWindow(notification.data.url);
        }
        notification.close();
      })
  );
});

workbox.routing.registerRoute(
  new RegExp('https://e2018.aseanskills2018.com/api/v1/.*'),
  workbox.strategies.networkFirst()
);
