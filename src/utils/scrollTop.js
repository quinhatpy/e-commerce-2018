const scrollTop = () => {
    let timeOut = setInterval(() => {
        if (window.scrollY === 0)
            clearTimeout(timeOut);
        window.scrollTo(0, window.scrollY - 80);
    }, 3)
}
export default scrollTop;
