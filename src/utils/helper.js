export const formatMoney = (amount, decimalCount = 0, decimal = ",", thousands = ".") => {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

export const countPercentSaleOff = (price, new_price) => {
    return '-' + (100 - Math.round((new_price / price) * 100)) + '%';
}

export const getPageMeta = (breadcrumb) => {
    const pageMeta = {
        title: '',
        description: ''
    }
    if (breadcrumb.length > 0) {
        const item = breadcrumb[breadcrumb.length - 1];
        pageMeta.title = item.seo_title ? item.seo_title : item.name;
        // pageMeta.description = item.seo_description;
    }
    return pageMeta;
}

export const urlBase64ToUint8Array = (base64String) => {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

export const onErrorImage = (event) => {
    event.target.onerror = null; 
    event.target.src = "/images/no-image.png"
}