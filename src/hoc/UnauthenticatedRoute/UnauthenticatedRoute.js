import PropTypes from 'prop-types';
import queryString from 'query-string';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

const UnauthenticatedRoute = ({ component: Component, ...rest }) => {
    let query = queryString.parse(rest.location.search);

    return (
        <Route
            {...rest}
            render={props => {
                // console.log(rest.isAuthenticated);
                return !rest.isAuthenticated ? (
                    <Component {...props} />
                ) : (
                        <Redirect to={query.redirect || '/'} />
                    )
            }

            }
        />
    );
};

UnauthenticatedRoute.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(
    mapStateToProps,
    null
)(UnauthenticatedRoute);
