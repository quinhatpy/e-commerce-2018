import PropTypes from 'prop-types';
import React, { Component } from 'react';
import TabContent from './TabContent';
import TabHeader from './TabHeader';

export default class ProductTab extends Component {
    constructor(props) {
        super(props)

        this.state = {
            tabActive: 0,
            tabNames: ['Bán chạy nhất', 'Mới về', 'Giảm giá'],
        }
    }

    handleChangeActiveTab(index) {
        this.setState({ tabActive: index })
    }

    render() {
        const { tabNames, tabActive } = this.state;
        const {
            productBoxes,
            onAddToCart
        } = this.props;
        
        return (
            <div className="product-tab12">
                <div className="title-tab12">
                    <TabHeader
                        tabNames={tabNames}
                        tabActive={tabActive}
                        changeActiveTab={this.handleChangeActiveTab.bind(this)} />
                </div>
                <div className="tab-content">
                    <TabContent
                        tabActive={tabActive}
                        tabNames={tabNames}
                        productBoxes={productBoxes}
                        onAddToCart={onAddToCart} />
                </div>
            </div>
        )
    }
}

ProductTab.propTypes = {
    productBoxes: PropTypes.object.isRequired,
    onAddToCart: PropTypes.func.isRequired
}
