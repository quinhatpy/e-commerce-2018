import PropTypes from 'prop-types';
import React, { Component } from 'react';

export default class TabHeader extends Component {
    renderItemTab(tabNames, tabActive, changeActiveTab) {
        return tabNames.map((item, index) => (
            <li className={tabActive === index ? 'active' : ''}
                key={index}>
                <span className="link" data-toggle="tab"
                    onClick={() => changeActiveTab(index)}>
                    {item}
                </span>
            </li>
        ))
    }

    render() {
        const {
            tabNames,
            tabActive,
            changeActiveTab
        } = this.props;

        return (
            <ul className="list-none">
                {this.renderItemTab(tabNames, tabActive, changeActiveTab)}
            </ul>
        )
    }
}

TabHeader.propTypes = {
    tabNames: PropTypes.array.isRequired,
    tabActive: PropTypes.number.isRequired,
    changeActiveTab: PropTypes.func.isRequired
}
