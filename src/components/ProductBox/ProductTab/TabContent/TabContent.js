import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import Slider from "react-slick";
import ItemProduct from '../../../ItemProduct';

const settings = {
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

export default class TabContent extends Component {
    renderTabContent(tabNames, tabActive, productBoxes) {
        const tabContents = ['selling_products', 'new_products', 'promotion_products'];

        return tabNames.map((item, index) => {
            const classes = ['tab-pane'];

            if (tabActive === index)
                classes.push('active');

            const data = Object.keys(productBoxes).length > 0 ? productBoxes[tabContents[index]] : null;
            return (
                <div className={classes.join(' ')}
                    key={index}>
                    <div className="product-slider12">
                        <div className="wrap-item1" >
                            <div>
                                {
                                    isEmpty(data) ? <p>Hiện không có sản phẩm</p>
                                        : (
                                            <Slider {...settings}>
                                                {
                                                    data ? this.renderItemProduct(data) : null
                                                }
                                            </Slider>
                                        )
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    }

    renderItemProduct(products) {
        if (products.length === 0)
            return <p>Hiện không có sản phẩm</p>

        return products.map(product => (
            <ItemProduct key={product.id} product={product} onAddToCart={this.props.onAddToCart} />
        ));
    }

    render() {
        const {
            tabNames,
            tabActive,
            productBoxes
        } = this.props;

        return (
            <Fragment>
                {this.renderTabContent(tabNames, tabActive, productBoxes)}
            </Fragment>
        )
    }
}

TabContent.propTypes = {
    tabNames: PropTypes.array.isRequired,
    tabActive: PropTypes.number.isRequired,
    productBoxes: PropTypes.object.isRequired,
    onAddToCart: PropTypes.func.isRequired
}
