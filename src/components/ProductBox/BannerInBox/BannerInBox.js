import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import ItemBannerInBox from './ItemBannerInBox';

export default class BannerInBox extends Component {
    render() {
        const { categoryBanner } = this.props;

        return (
            <Fragment>
                <div className="col-lg-10 col-md-9 col-sm-12">
                    <div className="banner-box banner-box12">
                        <ItemBannerInBox classes="link-banner-box" link={categoryBanner.link} imageSrc={categoryBanner.image} />
                    </div>
                </div>
            </Fragment>
        )
    }
}

BannerInBox.propTypes = {
    categoryBanner: PropTypes.shape({
        link: PropTypes.string,
        image: PropTypes.string
    }).isRequired
}
