import PropTypes from 'prop-types';
import React, { Component } from 'react';

export default class ItemBannerInBox extends Component {
    render() {
        const { classes, link, imageSrc } = this.props;
        return (
            <a href={link} className={classes}><img src={imageSrc} alt="" /></a>
        )
    }
}

ItemBannerInBox.propTypes = {
    classes: PropTypes.string.isRequired,
    link: PropTypes.string,
    imageSrc: PropTypes.string
}
