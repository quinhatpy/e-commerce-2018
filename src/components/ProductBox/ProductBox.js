import PropTypes from 'prop-types';
import React, { Component } from 'react';
import BannerInBox from './BannerInBox';
import ListCategoryInBox from './ListCategoryInBox';
import ProductTab from './ProductTab';

export default class ProductBox extends Component {
    render() {
        const {
            productBoxColor,
            categoryName,
            categoryChildren,
            productBoxes,
            categoryBanner,
            onAddToCart
        } = this.props;
        const classes = `product-box12 ${productBoxColor}`;

        return (
            <div className={classes}>
                <div className="container">
                    <div className="header-box12">
                        <h2 className="title24">{categoryName}</h2>
                        <div className="row">
                            <div className="col-lg-2 col-md-3 col-sm-12">
                                <ListCategoryInBox categories={categoryChildren} />
                            </div>
                            <BannerInBox categoryBanner={categoryBanner} />
                        </div>
                    </div>
                    <ProductTab productBoxes={productBoxes} onAddToCart={onAddToCart} />
                </div>
            </div>
        )
    }
}

ProductBox.propTypes = {
    productBoxColor: PropTypes.string.isRequired,
    categoryName: PropTypes.string.isRequired,
    categoryChildren: PropTypes.array.isRequired,
    productBoxes: PropTypes.object.isRequired,
    categoryBanner: PropTypes.object.isRequired,
    onAddToCart: PropTypes.func.isRequired
}
