import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ItemCategoryInBox from './ItemCategoryInBox';

export default class ListCategoryInBox extends Component {
    renderItemCategoryInBox(categories) {
        return categories.map(category => (
            <ItemCategoryInBox key={category.id} category={category} />
        ))
    }

    render() {
        const { categories } = this.props;

        return (
            <ul className="list-none list-cat12">
                {this.renderItemCategoryInBox(categories)}
            </ul>
        )
    }
}

ListCategoryInBox.propTypes = {
    categories: PropTypes.array.isRequired
}
