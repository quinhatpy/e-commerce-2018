import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../../../constants/link';

export default class ItemCategoryInBox extends Component {
    render() {
        const { category } = this.props;

        return (
            <li>
                <Link to={link.PRODUCT_LIST + category.slug}>
                    {category.icon ? <img src={category.icon} alt={category.name} /> : null}
                    <span>{category.name}</span>
                </Link>
            </li>
        )
    }
}

ItemCategoryInBox.propTypes = {
    category: PropTypes.shape({
        slug: PropTypes.string.isRequired,
        icon: PropTypes.string,
        name: PropTypes.string.isRequired
    }).isRequired
}
