import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Slider from "react-slick";

const settings = {
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
    ]
};

export default class GalleryThumbnail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            defaultImage: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.images)
            this.setState({ defaultImage: nextProps.images.length > 0 ? nextProps.images[0].path : ''})
    }

    renderImageItem(images) {
        if (images)
            return images.map(image => (
                <img key={image.id}
                    src={image.path}
                    onClick={this.handleChangeImage.bind(this, image.path)}
                    alt="" />
            ));
    }

    handleChangeImage(image) {
        this.setState({ defaultImage: image })
    }

    render() {
        const { images } = this.props;
        const { defaultImage } = this.state;

        return (
            <div className="detail-gallery">
                <div className="mid" style={{ height: '330px' }}>
                    <img src={defaultImage} alt="" />
                </div>
                <div className="gallery-control">
                    <div className="gallery-thumbnail wrap-item1">
                        <div>
                            <Slider {...settings}>
                                {this.renderImageItem(images)}
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

GalleryThumbnail.propTypes = {
    images: PropTypes.array
}