import PropTypes from 'prop-types';
import React, { Component } from 'react';

export default class ProductBranch extends Component {
    constructor(props) {
        super(props)

        this.state = {
            provinces: [],
            currentBranches: []
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.branches) {
            let provinces = [];

            nextProps.branches.forEach(branch => {
                const provinceIndex = provinces.findIndex(province => province.id === branch.province_id);
                if (provinceIndex === -1)
                    provinces.push({
                        id: branch.province_id,
                        name: `${branch.province_type} ${branch.province_name}`
                    })
            });

            let currentBranches = [];
            if (provinces.length > 0)
                currentBranches = nextProps.branches.filter(branch => branch.province_id === provinces[0].id)
            this.setState({ provinces, currentBranches })
        }
    }

    renderProvinceItem(provinces) {
        return provinces.map(province => (
            <option key={province.id} value={province.id}>{province.name}</option>
        ))
    }

    renderBranchItem(branches) {
        return branches.map(branch => (
            <li key={branch.id}><b>{branch.district_type + ' ' + branch.district_name}</b> {`${branch.branch_name} -  ${branch.address}, ${branch.ward_type} ${branch.ward_name}`}</li>
        ))
    }

    handleChangeProvince(event) {
        const provinceId = parseInt(event.target.value);
        const currentBranches = this.props.branches.filter(branch => branch.province_id === provinceId);

        this.setState({ currentBranches })
    }


    render() {
        const { provinces, currentBranches } = this.state;

        return (
            <div className="widget widget-seller">
                <h2 className="widget-title title14">Chi nhánh còn hàng</h2>
                <div className="widget-content" style={{ padding: '10px' }}>
                    <div className="attr-detail">
                        {provinces.length !== 0 ? <h5>Chọn tỉnh/thành phố gần bạn</h5> : null}
                        <div className="show-bar select-box" style={{ width: '100%' }}>
                            {
                                provinces.length === 0 ?
                                    <p>Sản phẩm đã hết hàng trên toàn bộ hệ thống</p> :
                                    (
                                        <select onChange={this.handleChangeProvince.bind(this)}>
                                            {this.renderProvinceItem(provinces)}
                                        </select>
                                    )
                            }

                        </div>
                    </div>
                    <ul>
                        {this.renderBranchItem(currentBranches)}
                    </ul>
                </div>
            </div>
        )
    }
}

ProductBranch.propTypes = {
    branches: PropTypes.array,
}
