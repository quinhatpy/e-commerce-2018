import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../constants/link';
import MiniCartContent from '../CartBox/MiniCartContent';
import Logo from '../Logo';
import ListItemMenu from '../MainNavigation/ListItemMenu';

export default class HeaderOnTop extends Component {
    render() {
        const {
            menus,
            cart,
            onUpdateCartItem,
            onRemoveCartItem,
            searchKeyword,
            onSearch,
            changeSearchKeyword,
            isAuthenticated,
            handleLogout
        } = this.props;

        return (
            <div className="header-ontop">
                <div className="container">
                    <div className="row">
                        <div className="col-md-2 col-sm-2 col-xs-12">
                            <Logo />
                        </div>
                        <div className="col-md-8 col-sm-8 col-xs-12">
                            <nav className="main-nav main-nav-ontop">
                                <ListItemMenu menus={menus} />
                            </nav>
                        </div>
                        <div className="col-md-2 col-sm-2 col-xs-12">
                            <div className="check-cart check-cart-ontop">
                                <div className="checkout-box">
                                    <span className="checkout-link link"><i className="fa fa-lock" aria-hidden="true" /> </span>
                                    {isAuthenticated ?
                                        (
                                            <ul className="list-checkout list-unstyled">
                                                <li><Link to={link.ACCOUNT}><i className="fa fa-user" />Tài khoản</Link></li>
                                                <li><Link to={link.MY_ORDER}><i className="fa fa-key" aria-hidden="true" />Đơn hàng</Link></li>
                                                <li><span className="link"
                                                    onClick={handleLogout}><i className="fa fa-sign-in" /> Đăng xuất</span></li>
                                            </ul>
                                        ) :
                                        (
                                            <ul className="list-checkout list-unstyled">
                                                <li><Link to={link.LOGIN}><i className="fa fa-user" /> Đăng nhập</Link></li>
                                                <li><Link to={link.REGISTER}><i className="fa fa-key" aria-hidden="true" />Đăng ký</Link></li>
                                            </ul>
                                        )}
                                </div>
                                <div className="search-hover-box">
                                    <span className="search-hover-link link"><i className="fa fa-search" aria-hidden="true" /> </span>
                                    <form className="search-form-hover" onSubmit={onSearch}>
                                        <input placeholder="Search..." type="text"
                                            defaultValue={searchKeyword}
                                            onChange={changeSearchKeyword}
                                        />
                                    </form>
                                </div>
                                <div className="mini-cart-box">
                                    <a className="mini-cart-link" href="cart.html">
                                        <span className="mini-cart-icon"><i className="fa fa-shopping-basket" aria-hidden="true" /></span>
                                        <span className="mini-cart-number">{cart.length}</span>
                                    </a>
                                    <MiniCartContent cart={cart} onUpdateCartItem={onUpdateCartItem} onRemoveCartItem={onRemoveCartItem} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

HeaderOnTop.propTypes = {
    menus: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    onUpdateCartItem: PropTypes.func.isRequired,
    onRemoveCartItem: PropTypes.func.isRequired,
    searchKeyword: PropTypes.string,
    onSearch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    handleLogout: PropTypes.func.isRequired
}
