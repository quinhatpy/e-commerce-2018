import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Logo extends Component {
    render() {
        return (
            <div className="logo logo2">
                <h1 className="hidden">E-Commerce 2018</h1>
                <Link to="/"><img src="/images/logo.png" alt="Ecommerce 2018" /> </Link>
            </div>
        )
    }
}
