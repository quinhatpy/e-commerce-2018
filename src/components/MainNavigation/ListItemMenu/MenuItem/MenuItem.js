import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class MenuItem extends Component {
    render() {
        const { name, link, hasChildren, children } = this.props;
        const classes = hasChildren ? 'menu-item-has-children' : '';

        return (
            <li className={classes}>
                <Link to={link}>{name}</Link>
                {
                    hasChildren ?
                        (<ul className="sub-menu">
                            {children}
                        </ul>
                        ) : null
                }
            </li>
        )
    }
}

MenuItem.propTypes = {
    name: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    hasChildren: PropTypes.bool.isRequired,
    children: PropTypes.array.isRequired,
}
