import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MenuItem from './MenuItem';

export default class ListItemMenu extends Component {
    renderMenuItem(menus, parentId = null) {
        let out = [];
        for (let i in menus) {
            if (menus[i].parent_id === parentId) {
                const children = menus.filter(menu => {
                    return parseInt(menu.parent_id) === menus[i].id;
                });

                const hasChildren = children.length > 0;
                out.push(
                    <MenuItem
                        name={menus[i].name}
                        link={menus[i].link}
                        key={menus[i].id}
                        hasChildren={hasChildren}>
                        {this.renderMenuItem(menus, menus[i].id)}
                    </MenuItem>
                )
            }
        }
        return out;
    }

    render() {
        const { menus } = this.props;

        return (
            <ul>
                <li>
                    <Link to="/">Trang chủ</Link>
                </li>
                {this.renderMenuItem(menus)}
            </ul>
        )
    }
}

ListItemMenu.propTypes = {
    menus: PropTypes.array.isRequired
}
