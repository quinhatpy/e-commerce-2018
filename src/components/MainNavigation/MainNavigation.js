import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ListItemMenu from './ListItemMenu';

export default class MainNavigation extends Component {
    render() {
        const { menus } = this.props;
        return (
            <div className="nav-search9">
                <nav className="main-nav main-nav12">
                    <ListItemMenu menus={menus} />
                    <span className="toggle-mobile-menu link"><span /> </span>
                </nav>
            </div>
        )
    }
}

MainNavigation.propTypes = {
    menus: PropTypes.array.isRequired
}
