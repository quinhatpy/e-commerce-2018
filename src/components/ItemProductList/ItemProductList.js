import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../constants/link';
import { formatMoney } from '../../utils/helper';

export default class ItemProductList extends Component {
    handleAddToCart() {
        const { product, onAddToCart } = this.props;

        const cartItem = {
            id: product.id,
            name: product.name,
            thumbnail: product.thumbnail,
            slug: product.slug,
            price: product.promotion ? product.promotion.new_price : product.price,
            quantity: 1
        }
        onAddToCart(cartItem);
    }

    render() {
        const { product } = this.props;

        return (
            <div className="item-product-list">
                <div className="row">
                    <div className="col-md-3 col-sm-4 col-xs-12">
                        <div className="item-pro-color">
                            <div className="product-thumb">
                                <Link to={link.PRODUCT_DETAIL + product.slug} className="product-thumb-link">
                                    <img src={product.thumbnail} className="active" alt={product.name} />
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9 col-sm-8 col-xs-12">
                        <div className="product-info">
                            <h3 className="product-title"><Link to={link.PRODUCT_DETAIL + product.slug}>{product.name}</Link></h3>
                            <div className="product-price">
                                {
                                    product.promotion ?
                                        (
                                            <Fragment>
                                                <ins><span>{formatMoney(product.promotion.new_price)}</span></ins>
                                                <del><span>{formatMoney(product.price)}</span></del>
                                            </Fragment>
                                        )
                                        :
                                        (
                                            <ins><span>{formatMoney(product.price)}</span></ins>
                                        )

                                }

                            </div>
                            <p className="desc" dangerouslySetInnerHTML={{ __html: product.description }}></p>
                            <div className="product-rate">
                                <div className="product-rating" style={{ width: `${product.rating === null ? 100 : ((product.rating * 100) / 5)}%` }} />
                            </div>
                            <div className="product-extra-link2">
                                <span onClick={this.handleAddToCart.bind(this)} className="addcart-link link">Add to Cart</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ItemProductList.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        slug: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        description: PropTypes.string,
        price: PropTypes.number.isRequired,
        promotion: PropTypes.shape({
            new_price: PropTypes.number.isRequired
        }),
        rating: PropTypes.number,
    }).isRequired,
    onAddToCart: PropTypes.func.isRequired
}
