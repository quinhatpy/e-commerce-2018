import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../constants/link';

export default class Breadcrumb extends Component {
    renderBreadcrumb(breadcrumb) {
        return breadcrumb.map((item, index) => {
            if (item.slug)
                return <Link key={index} to={link.PRODUCT_LIST + item.slug}>{item.name}</Link>
            return <span key={index}>{item.name}</span>
        });
    }
    render() {
        const { breadcrumb } = this.props;

        return (
            <div className="bread-crumb radius">
                <Link to="/">Trang chủ</Link>
                {this.renderBreadcrumb(breadcrumb)}
            </div>
        )
    }
}

Breadcrumb.propTypes = {
    breadcrumb: PropTypes.array.isRequired
}
