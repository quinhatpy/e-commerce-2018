import PropTypes from 'prop-types';
import React, { Component } from 'react';

export default class SearchBox extends Component {
    renderListCategory(categories) {
        const { changeCategorySelect } = this.props;

        return categories.map(category => {
            if (category.parent_id === null)
                return <li key={category.id}>
                    <span className="link"
                        onClick={() => changeCategorySelect(category.id)}>{category.name}</span>
                </li>
            return null;
        });
    }

    renderCategorySelected(categorySelected) {
        const { categories } = this.props;

        if (categorySelected === 0) return 'All Categories';
        const category = categories.find(category => category.id === categorySelected)
        if (category)
            return category.name;
        return null;
    }

    render() {
        const {
            categories,
            categorySelected,
            searchKeyword,
            changeCategorySelect,
            changeSearchKeyword,
            onSearch
        } = this.props;

        return (
            <div className="smart-search smart-search2">
                <div className="select-category">
                    <span className="category-toggle-link link">{this.renderCategorySelected(categorySelected)}</span>

                    <ul className="list-category-toggle list-unstyled">
                        <li><span className="link"
                            onClick={() => changeCategorySelect(0)}>All Categories</span></li>
                        {this.renderListCategory(categories)}
                    </ul>
                </div>
                <form className="smart-search-form" onSubmit={onSearch}>
                    <input type="text" placeholder="Search..."
                        defaultValue={searchKeyword}
                        onChange={changeSearchKeyword} />
                    <div className="submit-form">
                        <input className="radius" type="submit" value="" />
                    </div>
                </form>
            </div >
        )
    }
}

SearchBox.propTypes = {
    categories: PropTypes.array.isRequired,
    categorySelected: PropTypes.number.isRequired,
    searchKeyword: PropTypes.string,
    changeCategorySelect: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired
}
