import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Slider from "react-slick";
import ItemBannerSlider from './ItemBannerSlider';

const settings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    width: 500,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }
    ]
};
export default class BannerSlider extends Component {
    renderItemBanner(banners) {
        return banners.map(homeBanner => (
            <ItemBannerSlider
                key={homeBanner.id}
                image={homeBanner.image}
                link={homeBanner.link} />
        ))
    }

    render() {
        const { banners } = this.props;

        return (
            <div className="banner-slider banner-slider">
                <div className="wrap-item1">
                    <div>
                        <Slider {...settings}>
                            {this.renderItemBanner(banners)}
                        </Slider>
                    </div>
                </div>
            </div>
        )
    }
}

BannerSlider.propTypes = {
    banners: PropTypes.array.isRequired,
}
