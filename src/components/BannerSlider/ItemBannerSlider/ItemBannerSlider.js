import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ItemBannerSlider extends Component {
    render() {
        const {
            image,
            link
        } = this.props;

        return (
            <div className="item-banner">
                <div className="banner-thumb">
                    <Link to={link}><img src={image} alt="banner" /></Link>
                </div>
            </div>
        )
    }
}

ItemBannerSlider.propTypes = {
    image: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
}
