import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Pagination from '../Pagination';
import ItemPost from './ItemPost';

export default class ContentBlog extends Component {
    renderItemPost(articles) {
        if(articles.length === 0)
            return <p>Hiện không có bài viết.</p>
            
        return articles.map(article => (
            <ItemPost key={article.id} article={article} />
        ))
    }

    render() {
        const {
            articles,
            totalRecord,
            limit,
            currentPage,
            offset,
            link,
            onPageChange
        } = this.props;

        return (
            <div className="content-blog-small">
                {this.renderItemPost(articles)}
                <Pagination
                    totalRecord={totalRecord}
                    limit={limit}
                    currentPage={currentPage}
                    offset={offset}
                    link={link}
                    onPageChange={onPageChange} />
            </div>
        )
    }
}

ContentBlog.propTypes = {
    articles: PropTypes.array.isRequired,
    totalRecord: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    offset: PropTypes.number.isRequired,
    link: PropTypes.string.isRequired,
    onPageChange: PropTypes.func.isRequired
}
