import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../../constants/link';
import { onErrorImage } from '../../../utils/helper';
export default class ItemPost extends Component {
    render() {
        const { article } = this.props;

        return (
            <div className="item-post-small">
                <div className="row">
                    <div className="col-md-5 col-sm-6 col-xs-12">
                        <div className="post-thumb">
                            <div className="wrap-item1">
                                <Link to={link.ARTICLE_DETAIL + article.slug} className="post-thumb-link">
                                    <img src={article.thumbnail} alt={article.name} onError={onErrorImage}/>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-7 col-sm-6 col-xs-12">
                        <div className="post-info">
                            <ul className="post-date-comment">
                                <li><i className="fa fa-calendar-o" aria-hidden="true" /><span>{article.created_at}</span></li>
                            </ul>
                            <h3 className="post-title"><Link to={link.ARTICLE_DETAIL + article.slug}>{article.name}</Link></h3>
                            <p className="desc">{article.short_description}</p>
                            <ul className="post-date-comment">
                                <li><i className="fa fa-user" aria-hidden="true" /><label>Đăng bởi:</label>{article.user.name}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

ItemPost.propTypes = {
    article: PropTypes.shape({
        slug: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        created_at: PropTypes.string.isRequired,
        short_description: PropTypes.string.isRequired,
        user: PropTypes.object.isRequired,
    }).isRequired,
}
