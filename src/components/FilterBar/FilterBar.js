import PropTypes from 'prop-types';
import React, { Component } from 'react';

export default class FilterBar extends Component {
    render() {
        const { onSort } = this.props;

        return (
            <div className="sort-pagi-bar clearfix">
                <div className="sort-paginav pull-right">
                    <div className="sort-bar select-box">
                        <label>Sắp xếp theo:</label>
                        <select onChange={onSort}>
                            <option value="-id">Mới nhất</option>
                            <option value="-view">Lượt xem</option>
                            <option value="-sold">Bán chạy</option>
                            <option value="price">Giá tăng dần</option>
                            <option value="-price">Giá giảm dần</option>
                            <option value="name">Tên tăng dần</option>
                            <option value="-name">Tên giảm dần</option>
                        </select>
                    </div>
                </div>
            </div>
        )
    }
}

FilterBar.propTypes = {
    onSort: PropTypes.func.isRequired,
}
