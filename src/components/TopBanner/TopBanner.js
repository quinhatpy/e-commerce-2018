import React, { Component } from 'react';

export default class TopBanner extends Component {
    render() {
        return (
            <div className="top-banner12">
                <div className="container">
                    <div className="inner-top-banner12">
                        <ul className="list-text12 list-none">
                            <li>
                                <p>Birthday kuteshop 1 year!</p>
                            </li>
                            <li><h2 className="title30 color">foot flair</h2></li>
                            <li>
                                <p>Anything-but-basic shoes</p>
                                <strong className="color">On sale December 28-30</strong>
                            </li>
                        </ul>
                        <img src="images/home12/bn-top.png" alt="" />
                        <a href="/" className="shopnow title14 bg-color radius">Shop now</a>
                    </div>
                </div>
            </div>
        )
    }
}
