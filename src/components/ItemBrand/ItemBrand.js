import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ItemBrand extends Component {
    render() {
        const { brand } = this.props;

        return (
            <div className="item-brand10">
                <Link to={brand.slug}><img src={brand.logo} alt={brand.name} /></Link>
            </div>
        )
    }
}

ItemBrand.propTypes = {
    brand: PropTypes.shape({
        slug: PropTypes.string.isRequired,
        logo: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired
    }).isRequired
}
