import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ItemProductInList from '../ItemProductInList';
import Pagination from '../Pagination';

export default class GridProduct extends Component {
    renderItemProduct(products) {
        if (products.length === 0)
            return <p>Hiện không có sản phẩm.</p>
            
        return products.map(product => (
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12"
                key={product.id}>
                <ItemProductInList product={product} onAddToCart={this.props.onAddToCart} />
            </div>
        ))
    }
    render() {
        const {
            products,
            totalRecord,
            limit,
            currentPage,
            offset,
            link,
            queryParams,
            onPageChange
        } = this.props;

        return (
            <div className="grid-pro-color">
                <div className="row">
                    {this.renderItemProduct(products)}
                </div>
                <Pagination
                    totalRecord={totalRecord}
                    limit={limit}
                    currentPage={currentPage}
                    offset={offset}
                    link={link}
                    queryParams={queryParams}
                    onPageChange={onPageChange} />
            </div>
        )
    }
}

GridProduct.propTypes = {
    products: PropTypes.array.isRequired,
    totalRecord: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    offset: PropTypes.number.isRequired,
    link: PropTypes.string.isRequired,
    queryParams: PropTypes.object.isRequired,
    onPageChange: PropTypes.func.isRequired,
    onAddToCart: PropTypes.func.isRequired
}
