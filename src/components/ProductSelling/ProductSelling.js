import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ItemProductSelling from './ItemProductSelling';

export default class ProductSelling extends Component {
    renderItemProduct(products) {
        if(products.length === 0)
            return <p>Hiện không có sản phẩm.</p>
            
        return products.map(product => (
            <ItemProductSelling key={product.id} product={product} />
        ))
    }
    render() {
        const { bestSelling } = this.props;

        return (
            <div className="widget widget-seller">
                <h2 className="widget-title title14">Sản phẩm bán nhiều nhất</h2>
                <div className="widget-content">
                    <div className="wrap-item1" >
                        <div className="list-pro-seller">
                            {this.renderItemProduct(bestSelling)}
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

ProductSelling.propTypes = {
    bestSelling: PropTypes.array.isRequired
}
