import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import queryString from 'query-string';

export default class Pagination extends Component {

    constructor(props) {
        super(props);

        this.state = {
            'currentPage': 1,
            'totalRecord': 1,
            'totalPage': 1,
            'limit': 10,
            'group': 10,
            'first': 1,
            'last': 1,
            'prevGroup': 1,
            'nextGroup': 1,
            'offset': 0,
            'link': '',
            'queryParams': ''
        }
        const config = { ...this.state };

        /*
         * Lặp qua từng phần tử config truyền vào và gán vào config của đối tượng
         * trước khi gán vào thì phải kiểm tra thông số config truyền vào có nằm
         * trong hệ thống config không, nếu có thì mới gán
         */
        for (let key in props) {
            config[key] = props[key];
        }

        /*
         * Kiểm tra thông số limit truyền vào có nhỏ hơn 0 hay không?
         * Nếu nhỏ hơn thì gán cho limit = 0
         */
        if (config.limit < 0) config.limit = 0;

        /*
         * Tính total page, công tức tính tổng số trang như sau:
         * total_page = ciel(total_record/limit).
         */
        config.totalPage = Math.ceil(config.totalRecord / config.limit);

        /*
         * Sau khi có tổng số trang ta kiểm tra xem nó có nhỏ hơn 0 hay không
         * nếu nhỏ hơn 0 thì gán nó băng 1 ngay. Vì mặc định tổng số trang luôn bằng 1
         */
        if (!config.totalPage) config.totalPage = 1;

        /*
         * Trang hiện tại sẽ rơi vào một trong các trường hợp sau:
         *  - Nếu người dùng truyền vào số trang nhỏ hơn 1 thì ta sẽ gán nó = 1
         *  - Nếu trang hiện tại người dùng truyền vào lớn hơn tổng số trang
         *    thì ta gán nó bằng tổng số trang
         */
        if (config.currentPage < 1) config.currentPage = 1;
        if (config.currentPage > config.totalPage) config.currentPage = config.totalPage;

        config.queryParams = queryString.stringify(config.queryParams);
        this.calPrevGroupAndNextGroup(config);
        this.calFirstPageAndLastPage(config);

        this.state = config;

    }

    componentWillReceiveProps(nextProps) {
        const config = { ...this.state };
        if (nextProps.currentPage !== this.props.currentPage || nextProps.totalRecord !== this.props.totalRecord) {
            config.currentPage = nextProps.currentPage;
            config.totalRecord = nextProps.totalRecord;
            config.totalPage = Math.ceil(config.totalRecord / config.limit);
            config.offset = nextProps.offset;

            this.calPrevGroupAndNextGroup(config);
            this.calFirstPageAndLastPage(config);
        }

        config.queryParams = queryString.stringify(nextProps.queryParams);
        this.setState(config);
    }


    calPrevGroupAndNextGroup(config) {
        /*
        * Tinh prevGroup, nextGroup
        */
        config.prevGroup = Math.floor((config.currentPage - 1) / config.group) * config.group + 1 - config.group;
        config.nextGroup = Math.floor((config.currentPage - 1) / config.group) * config.group + 1 + config.group;
    }

    calFirstPageAndLastPage(config) {
        /*
        * Tính first, last
        */

        config.first = Math.floor((config.currentPage - 1) / config.group) * config.group + 1;
        config.last = config.first + config.group - 1;

        if (config.last > config.totalPage) config.last = config.totalPage;
    }

    getLink(page) {

        return this.state.link + '/' + page;
        // return this.state.link + page + '?' + this.state.queryParams;
    }

    renderFirstPage() {
        if (this.state.currentPage > 1) {
            const label = <Fragment>
                <span aria-hidden="true">Trang đầu</span>
                <span className="sr-only">Trang đầu</span>
            </Fragment>

            return (
                <NavLink
                    className="pageCustom"
                    to={{
                        pathname: this.getLink(1),
                        search: this.state.queryParams
                    }}
                    onClick={() => this.props.onPageChange(1, this.state.queryParams)}
                >
                    {label}
                </NavLink>
            )
        }
    }

    renderLastPage() {
        if (this.state.currentPage < this.state.totalPage) {
            const label = <Fragment>
                <span aria-hidden="true">Trang cuối</span>
                <span className="sr-only">Trang cuối</span>
            </Fragment>

            return (
                <NavLink
                    className="pageCustom"
                    to={{
                        pathname: this.getLink(this.state.totalPage),
                        search: this.state.queryParams
                    }}
                    onClick={() => this.props.onPageChange(this.state.totalPage, this.state.queryParams)}
                >
                    {label}
                </NavLink>
            )
        }
    }

    renderPreviousGroup() {
        if (this.state.currentPage > this.state.group) {
            const label = <Fragment>
                <span aria-hidden="true" className="fa fa-chevron-left"></span>
                <span className="sr-only">Nhóm trước</span>
            </Fragment>

            return (
                <NavLink
                    to={{
                        pathname: this.getLink(this.state.prevGroup),
                        search: this.state.queryParams
                    }}
                    onClick={() => this.props.onPageChange(this.state.prevGroup, this.state.queryParams)}
                >
                    {label}
                </NavLink>
            )
        }
    }

    renderNextGroup() {
        if (this.state.nextGroup <= this.state.totalPage) {
            const label = <Fragment>
                <span aria-hidden="true" className="fa fa-chevron-right"></span>
                <span className="sr-only">Nhóm tiếp theo</span>
            </Fragment>

            return (
                <NavLink
                    to={{
                        pathname: this.getLink(this.state.nextGroup),
                        search: this.state.queryParams
                    }}
                    onClick={() => this.props.onPageChange(this.state.nextGroup, this.state.queryParams)}
                >
                    {label}
                </NavLink>
            )
        }
    }

    renderPreviousPage() {
        if (this.state.currentPage > 1) {
            const label = <Fragment>
                <span aria-hidden="true">«</span>
                <span className="sr-only">Trang trước</span>
            </Fragment>

            return (
                <NavLink
                    to={{
                        pathname: this.getLink(this.state.currentPage - 1),
                        search: this.state.queryParams
                    }}
                    onClick={() => this.props.onPageChange(this.state.currentPage - 1, this.state.queryParams)}
                >
                    {label}
                </NavLink>
            )
        }
    }

    renderNextPage() {
        if (this.state.currentPage < this.state.totalPage) {
            const label = <Fragment>
                <span aria-hidden="true">»</span>
                <span className="sr-only">Trang sau</span>
            </Fragment>
            return (
                <NavLink
                    to={{
                        pathname: this.getLink(this.state.currentPage + 1),
                        search: this.state.queryParams
                    }}
                    onClick={() => this.props.onPageChange(this.state.currentPage + 1, this.state.queryParams)}
                >
                    {label}
                </NavLink>
            )
        }
    }

    renderThreeDots(position) {
        if (position === 'after' && this.state.nextGroup <= this.state.totalPage)
            return (
                <span>
                    <span className="fa fa-ellipsis-h"></span>
                </span>
            )
        if (position === 'before' && this.state.currentPage >= this.state.group && this.state.last > this.state.limit)
            return (
                <span>
                    <span className="fa fa-ellipsis-h"></span>
                </span>
            )
    }

    renderPageNumber() {
        let arrNumber = [];
        for (let i = this.state.first; i <= this.state.last; i++) {
            arrNumber.push(i);
        }

        return arrNumber.map(pageNumber => {
            const classes = this.state.currentPage === pageNumber ? 'current-page' : '';

            return <NavLink
                key={pageNumber}
                to={{
                    pathname: this.getLink(pageNumber),
                    search: this.state.queryParams
                }}
                className={classes}
                activeClassName="current-page"
                onClick={this.props.onPageChange.bind(this, pageNumber, this.state.queryParams)}
            >
                {pageNumber}
            </NavLink>
        })
    }

    render() {
        const config = this.state;
        // console.log(config);

        // Kiểm tra tổng số trang lớn hơn 1 mới phân trang
        if (config.totalRecord > config.limit) {
            return (
                <div className="pagi-bar bottom">
                    {this.renderFirstPage()}
                    {this.renderPreviousGroup()}
                    {this.renderPreviousPage()}
                    {this.renderThreeDots('before')}
                    {this.renderPageNumber()}
                    {this.renderThreeDots('after')}
                    {this.renderNextPage()}
                    {this.renderNextGroup()}
                    {this.renderLastPage()}
                </div>
            )
        }
        return null;
    }
}

Pagination.propTypes = {
    totalRecord: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    offset: PropTypes.number.isRequired,
    link: PropTypes.string.isRequired,
    queryParams: PropTypes.object,
    onPageChange: PropTypes.func.isRequired,
}
