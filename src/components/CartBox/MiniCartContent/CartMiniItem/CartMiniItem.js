import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../../../constants/link';
import { formatMoney } from '../../../../utils/helper';

export default class CartMiniItem extends Component {
    render() {
        const {
            item,
            onRemoveCartItem,
            onUpdateCartItem
        } = this.props;

        return (
            <li>
                <div className="mini-cart-edit">
                    <span className="delete-mini-cart-item link" onClick={onRemoveCartItem}><i className="fa fa-trash-o" /> </span>
                </div>
                <div className="mini-cart-thumb">
                    <Link to={link.PRODUCT_DETAIL + item.slug}><img src={item.thumbnail} alt={item.name} /></Link>
                </div>
                <div className="mini-cart-info">
                    <h3><Link to={link.PRODUCT_DETAIL + item.slug}>{item.name}</Link></h3>
                    <div className="info-price">
                        <span>{formatMoney(item.price)}<sup>đ</sup></span>
                    </div>
                    <div className="qty-product">
                        <div className="detail-qty border radius">
                            <input type="number" defaultValue={item.quantity} min="1" onChange={onUpdateCartItem} />
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

CartMiniItem.propTypes = {
    item: PropTypes.shape({
        slug: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
    onUpdateCartItem: PropTypes.func.isRequired,
    onRemoveCartItem: PropTypes.func.isRequired,
}
