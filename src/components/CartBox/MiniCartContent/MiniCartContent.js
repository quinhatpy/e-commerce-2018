import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../../constants/link';
import { formatMoney } from '../../../utils/helper';
import CartMiniItem from './CartMiniItem';

export default class MiniCartContent extends Component {
    renderCartItem(cart) {
        return cart.map((item, index) => (
            <CartMiniItem
                key={item.id}
                item={item}
                onRemoveCartItem={this.handleRemoveCartItem.bind(this, index)}
                onUpdateCartItem={this.handleUpdateCartItem.bind(this, index)} />
        ));
    }

    countTotal(cart) {
        return cart.reduce((total, item) => total += item.price * item.quantity, 0)
    }

    handleRemoveCartItem(index) {
        if (window.confirm('Bạn có muốn xóa sản phẩm này ra khỏi giỏ hàng!'))
            this.props.onRemoveCartItem(index);
    }

    handleUpdateCartItem(index, event) {
        const quantity = isNaN(parseInt(event.target.value)) ? 1 : parseInt(event.target.value);
        this.props.onUpdateCartItem(index, quantity);
    }

    render() {
        const { cart } = this.props;

        return (
            <div className="mini-cart-content">
                <h2>({cart.length}) sản phẩm trong giỏ hàng</h2>
                <ul className="list-mini-cart-item list-unstyled">
                    {this.renderCartItem(cart)}
                </ul>
                {
                    cart.length > 0 ?
                        (
                            <Fragment>
                                <div className="mini-cart-total">
                                    <label>Tổng cộng</label>
                                    <span>{formatMoney(this.countTotal(cart))}<sup>đ</sup></span>
                                </div>
                                <div className="mini-cart-button">
                                    <Link className="mini-cart-view" to={link.CART}>Xem giỏ hàng </Link> &nbsp;
                                    <Link className="mini-cart-checkout" to={link.CHECKOUT}>Đặt hàng</Link>
                                </div>
                            </Fragment>
                        ) : null
                }
            </div>
        )
    }
}

MiniCartContent.propTypes = {
    cart: PropTypes.array.isRequired,
    onUpdateCartItem: PropTypes.func.isRequired,
    onRemoveCartItem: PropTypes.func.isRequired,
}
