import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../constants/link';
import MiniCartContent from './MiniCartContent';

export default class CartBox extends Component {
    render() {
        const {
            cart,
            onUpdateCartItem,
            onRemoveCartItem
        } = this.props;

        return (
            <div className="mini-cart-box mini-cart1">
                <Link className="mini-cart-link" to={link.CART}>
                    <span className="mini-cart-icon"><i className="fa fa-shopping-basket" aria-hidden="true" /></span>
                    <span className="mini-cart-number border radius">{cart.length}</span>
                </Link>
                <MiniCartContent cart={cart} onUpdateCartItem={onUpdateCartItem} onRemoveCartItem={onRemoveCartItem} />
            </div>
        )
    }
}

CartBox.propTypes = {
    cart: PropTypes.array.isRequired,
    onUpdateCartItem: PropTypes.func.isRequired,
    onRemoveCartItem: PropTypes.func.isRequired,
}
