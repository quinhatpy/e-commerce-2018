import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../constants/link';
import { formatMoney } from '../../utils/helper';

export default class CartItem extends Component {
    render() {
        const {
            index,
            item,
            onRemoveCartItem,
            onUpdateCartItem
        } = this.props;

        return (
            <tr className="cart_item">
                <td className="product-remove">
                    {index}
                </td>
                <td className="product-thumbnail">
                    <Link to={link.PRODUCT_DETAIL + item.slug}><img src={item.thumbnail} alt={item.name} /></Link>
                </td>
                <td className="product-name">
                    <Link to={link.PRODUCT_DETAIL + item.slug}>{item.name}</Link>
                </td>
                <td className="product-price">
                    <span className="amount">{formatMoney(item.price)}<sup>đ</sup></span>
                </td>
                <td className="product-quantity">
                    <div className="detail-qty border radius">
                        <input type="number" defaultValue={item.quantity} min="1"
                            onChange={onUpdateCartItem} />
                    </div>
                </td>
                <td className="product-subtotal">
                    <span className="amount">{formatMoney(item.price * item.quantity)}<sup>đ</sup></span>
                </td>
                <td className="product-remove">
                    <span className="remove link" onClick={onRemoveCartItem}><i className="fa fa-times" /></span>
                </td>
            </tr>

        )
    }
}

CartItem.propTypes = {
    index: PropTypes.number.isRequired,
    item: PropTypes.shape({
        slug: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
    onUpdateCartItem: PropTypes.func.isRequired,
    onRemoveCartItem: PropTypes.func.isRequired,
}
