import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../../constants/link';

export default class ItemCategoryMenu extends Component {
    checkHasGrandChildren(grandChildren) {
        let check = false;
        for (let key in grandChildren) {
            if (grandChildren[key].length > 0)
                check = true;
        }
        return check;
    }

    render() {
        const { category, categories } = this.props;
        const { name, slug, icon } = category;

        const children = categories.filter(item => {
            return parseInt(item.parent_id) === category.id
        });

        let grandChildren = {};

        children.forEach(element => {
            const grandChildrenEachChildren = categories.filter(item => {
                return parseInt(item.parent_id) === element.id;
            });

            if (grandChildrenEachChildren.length > 0)
                grandChildren[element.id] = grandChildrenEachChildren;
            else grandChildren[element.id] = [];
        });


        // khong co con - co con khong co chau - co con va co chau
        // console.log(name, children, grandChildren, this.checkHasGrandChildren(grandChildren))
        return (
            <Fragment>
                {
                    children.length === 0 ?
                        <li>
                            <Link to={link.PRODUCT_LIST + slug}><img alt={name} src={icon} /> <span>{name}</span></Link>
                        </li>
                        :
                        !this.checkHasGrandChildren(grandChildren) ?
                            (
                                <li className="has-cat-mega">
                                    <Link to={link.PRODUCT_LIST + slug}><img alt={name} src={icon} /> <span>{name}</span></Link>
                                    <div className="cat-mega-menu cat-mega-style0">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="list-cat-mega-menu">
                                                    <h2 className="title-cat-mega-menu">{name}</h2>
                                                    <ul>
                                                        {
                                                            children.map(item => (
                                                                <li key={item.id}>
                                                                    <Link to={link.PRODUCT_LIST + item.slug}>
                                                                        {item.icon ? <img alt={item.name} src={item.icon} /> : null}
                                                                        <span>{item.name}</span>
                                                                    </Link>
                                                                </li>
                                                            ))
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            )
                            :
                            (
                                <li className="has-cat-mega">
                                    <Link to={link.PRODUCT_LIST + slug}><img alt={name} src={icon} /> <span>{name}</span></Link>
                                    <div className="cat-mega-menu cat-mega-style1">
                                        <div className="row">
                                            {
                                                children.map(item => (
                                                    <div className="col-md-4 col-sm-3" key={item.id}>
                                                        <div className="list-cat-mega-menu">
                                                            <h2 className="title-cat-mega-menu">
                                                                <Link to={link.PRODUCT_LIST + item.slug}>{item.name}</Link>
                                                            </h2>
                                                            <ul>
                                                                {
                                                                    grandChildren[item.id].map(itemChild => (
                                                                        <li key={itemChild.id}>
                                                                            <Link to={link.PRODUCT_LIST + itemChild.slug}>
                                                                                {itemChild.icon ? <img alt={itemChild.name} src={itemChild.icon} /> : null}
                                                                                <span>{itemChild.name}</span>
                                                                            </Link>
                                                                        </li>
                                                                    ))
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>
                                                ))
                                            }
                                        </div>
                                    </div>
                                </li>
                            )
                }
            </Fragment>
        )
    }
}

ItemCategoryMenu.propTypes = {
    category: PropTypes.shape({
        name: PropTypes.string.isRequired,
        slug: PropTypes.string.isRequired,
        icon: PropTypes.string.isRequired
    }).isRequired,
    categories: PropTypes.array.isRequired
}
