import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ItemCategoryMenu from './ItemCategoryMenu';

export default class CategoryMenu extends Component {
    renderItemCategoryMenu(categories) {
        return categories.map(category => {
            if (category.parent_id === null)
                return <ItemCategoryMenu key={category.id} category={category} categories={categories} />
            return null;
        })
    }

    render() {
        const { categories } = this.props;

        return (
            <div className="wrap-cat-icon wrap-cat-icon12">
                <h2 className="title14 title-cat-icon">Danh mục sản phẩm </h2>
                <ul className="list-cat-icon">
                    {
                        this.renderItemCategoryMenu(categories)
                    }
                </ul>
            </div>
        )
    }
}

CategoryMenu.propTypes = {
    categories: PropTypes.array.isRequired
}
