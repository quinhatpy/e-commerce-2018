import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../constants/link';
import { formatMoney } from '../../utils/helper';

export default class ItemProductInList extends Component {
    handleAddToCart() {
        const { product, onAddToCart } = this.props;

        const cartItem = {
            id: product.id,
            name: product.name,
            thumbnail: product.thumbnail,
            slug: product.slug,
            price: product.promotion ? product.promotion.new_price : product.price,
            quantity: 1
        }
        onAddToCart(cartItem);
    }

    render() {
        const { product } = this.props;

        return (
            <div className="item-pro-color">
                <div className="product-thumb">
                    <Link to={link.PRODUCT_DETAIL + product.slug} className="product-thumb-link">
                        <img src={product.thumbnail} className="active" alt={product.name} />
                    </Link>
                </div>
                <div className="product-info">
                    <h3 className="product-title"><Link to={link.PRODUCT_DETAIL + product.slug}>{product.name}</Link></h3>
                    <div className="product-price">
                        {
                            product.promotion ?
                                (
                                    <Fragment>
                                        <ins><span>{formatMoney(product.promotion.new_price)}</span></ins>
                                        <del><span>{formatMoney(product.price)}</span></del>
                                    </Fragment>
                                )
                                :
                                (
                                    <ins><span>{formatMoney(product.price)}</span></ins>
                                )

                        }

                    </div>
                    <div className="product-extra-link">
                        <span className="addcart-link link" onClick={this.handleAddToCart.bind(this)}><i className="fa fa-shopping-basket" aria-hidden="true" /><span>Add to Cart</span></span>
                    </div>
                </div>
            </div>
        )
    }
}

ItemProductInList.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        slug: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        promotion: PropTypes.shape({
            new_price: PropTypes.number.isRequired
        })
    }).isRequired,
    onAddToCart: PropTypes.func
}
