import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Slider from "react-slick";
import ItemProductInList from '../ItemProductInList';

const settings = {
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

export default class ProductRelated extends Component {
    renderItemProduct(similarProducts) {
        if(similarProducts.length === 0)
            return <p>Hiện không có sản phẩm.</p>

        return similarProducts.map(product => (
            <ItemProductInList key={product.id} product={product} />
        ))
    }

    render() {
        const { similarProducts } = this.props;

        return (
            <div className="product-related border radius">
                <h2 className="title18">Sản phẩm tương tự</h2>
                <div className="product-related-slider">
                    <div className="wrap-item1">
                        <div>
                            <Slider {...settings}>
                                {this.renderItemProduct(similarProducts)}
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ProductRelated.propTypes = {
    similarProducts: PropTypes.array.isRequired,
}
