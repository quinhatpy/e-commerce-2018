import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ItemProductSuperDeal from './ItemProductSuperDeal';

export default class SuperDeal extends Component {
    renderItemProductDeals(superDeals) {
        if(superDeals.length === 0)
            return <p>Hiện không có sản phẩm</p>

        return superDeals.map(product => (
            <div className="list-pro-seller clearfix" key={product.id}>
                <ItemProductSuperDeal product={product} />
            </div>
        ))
    }

    render() {
        const { superDeals } = this.props;

        return (
            <div className="supper-deal12 top-review11">
                <h2 className="title14 title-top12">Siêu giảm giá</h2>
                <div className="wrap-item1">
                    {this.renderItemProductDeals(superDeals)}
                </div>
            </div>
        )
    }
}

SuperDeal.propTypes = {
    superDeals: PropTypes.array.isRequired
}
