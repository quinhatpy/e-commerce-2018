import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as link from '../../../constants/link';
import { countPercentSaleOff, formatMoney } from '../../../utils/helper';

export default class ItemProductSuperDeal extends Component {
    render() {
        const { product } = this.props;

        return (
            <div className="item-pro-seller">
                <div className="product-thumb">
                    <Link to={link.PRODUCT_DETAIL + product.slug} className="product-thumb-link"><img src={product.thumbnail} alt={product.name} /></Link>
                </div>
                <div className="product-info">
                    <h3 className="product-title" title={product.name}><Link to={link.PRODUCT_DETAIL + product.slug}>{product.name}</Link></h3>
                    <div className="product-price">
                        <ins><span>{formatMoney(product.new_price)}<sup>đ</sup></span></ins>
                        <del><span>{formatMoney(product.price)}<sup>đ</sup></span></del>
                        <span className="saleoff">{countPercentSaleOff(product.price, product.new_price)}</span>
                    </div>
                </div>
            </div>
        )
    }
}

ItemProductSuperDeal.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        slug: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        new_price: PropTypes.number.isRequired
    }).isRequired
}
