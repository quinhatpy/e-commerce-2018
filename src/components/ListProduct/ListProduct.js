import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ItemProductList from '../ItemProductList';
import Pagination from '../Pagination';

export default class ListProduct extends Component {
    renderItemProduct(products) {
        if(products.length === 0)
            return <p>Hiện không có sản phẩm.</p>

        return products.map(product => (
            <ItemProductList key={product.id} product={product} onAddToCart={this.props.onAddToCart} />
        ))
    }
    render() {
        const {
            products,
            totalRecord,
            limit,
            currentPage,
            offset,
            link,
            queryParams,
            onPageChange,
        } = this.props;

        return (
            <div className="grid-pro-color">

                {this.renderItemProduct(products)}

                <Pagination
                    totalRecord={totalRecord}
                    limit={limit}
                    currentPage={currentPage}
                    offset={offset}
                    link={link}
                    queryParams={queryParams}
                    onPageChange={onPageChange} />
            </div>
        )
    }
}

ListProduct.propTypes = {
    products: PropTypes.array.isRequired,
    totalRecord: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    offset: PropTypes.number.isRequired,
    link: PropTypes.string.isRequired,
    queryParams: PropTypes.object.isRequired,
    onPageChange: PropTypes.func.isRequired,
    onAddToCart: PropTypes.func.isRequired
}
