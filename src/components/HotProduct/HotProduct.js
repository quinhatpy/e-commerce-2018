import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Slider from "react-slick";
import ItemProduct from '../ItemProduct';

const settings = {
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

export default class HotProduct extends Component {
    renderItemProduct(products) {
        return products.map(product => (
            <ItemProduct key={product.id} product={product} onAddToCart={this.props.onAddToCart} />
        ));
    }

    render() {
        const { hotProducts } = this.props;

        return (
            <div className="hot-product12">
                <h2 className="title-hot12 title24 text-center"><span>Sản phẩm HOT</span></h2>
                <div className="product-slider12">
                    <div className="wrap-item1" >
                        <div>
                            {
                                isEmpty(hotProducts) ? <p>Hiện không có sản phẩm</p>
                                    : (
                                        <Slider {...settings}>
                                            {this.renderItemProduct(hotProducts)}
                                        </Slider>
                                    )
                            }

                        </div>

                    </div>
                </div>
            </div>

        )
    }
}

HotProduct.propTypes = {
    hotProducts: PropTypes.array.isRequired,
    onAddToCart: PropTypes.func.isRequired,
}
