import React, { Fragment } from 'react';
import Loadable from 'react-loadable';
import { Route, Switch } from 'react-router-dom';
import * as link from './constants/link';
import Footer from './containers/Footer';
import Header from './containers/Header';
import UnauthenticatedRoute from './hoc/UnauthenticatedRoute';
import NotFound from './pages/NotFound';

const HomePage = Loadable({
    loader: () => import(/* webpackChunkName: "homepage" */ './pages/HomePage'),
    loading: () => null,
    modules: ['homepage']
});

const ProductList = Loadable({
    loader: () => import(/* webpackChunkName: "product-list" */ './pages/ProductList'),
    loading: () => null,
    modules: ['product-list']
});

const ProductDetail = Loadable({
    loader: () => import(/* webpackChunkName: "product-detail" */ './pages/ProductDetail'),
    loading: () => null,
    modules: ['product-detail']
});

const SearchProduct = Loadable({
    loader: () => import(/* webpackChunkName: "search-product" */ './pages/SearchProduct'),
    loading: () => null,
    modules: ['search-product']
});

const Cart = Loadable({
    loader: () => import(/* webpackChunkName: "cart" */ './pages/Cart'),
    loading: () => null,
    modules: ['cart']
});

const ArticleList = Loadable({
    loader: () => import(/* webpackChunkName: "article-list" */ './pages/ArticleList'),
    loading: () => null,
    modules: ['article-list']
});

const ArticleDetail = Loadable({
    loader: () => import(/* webpackChunkName: "article-detail" */ './pages/ArticleDetail'),
    loading: () => null,
    modules: ['article-detail']
});

const Login = Loadable({
    loader: () => import(/* webpackChunkName: "article-detail" */ './pages/Login'),
    loading: () => null,
    modules: ['login']
});

const Account = Loadable({
    loader: () => import(/* webpackChunkName: "account" */ './pages/Account'),
    loading: () => null,
    modules: ['account']
});

const Register = Loadable({
    loader: () => import(/* webpackChunkName: "register" */ './pages/Register'),
    loading: () => null,
    modules: ['register']
});

const ResetPasswordRequest = Loadable({
    loader: () => import(/* webpackChunkName: "reset-password-request" */ './pages/ResetPasswordRequest'),
    loading: () => null,
    modules: ['reset-password-request']
});

const ResetPasswordConfirm = Loadable({
    loader: () => import(/* webpackChunkName: "reset-password-confirm" */ './pages/ResetPasswordConfirm'),
    loading: () => null,
    modules: ['reset-password-confirm']
});

const Checkout = Loadable({
    loader: () => import(/* webpackChunkName: "checkout" */ './pages/Checkout'),
    loading: () => null,
    modules: ['checkout']
});

const MyOrder = Loadable({
    loader: () => import(/* webpackChunkName: "my-order" */ './pages/MyOrder'),
    loading: () => null,
    modules: ['my-order']
});

const MyOrderDetail = Loadable({
    loader: () => import(/* webpackChunkName: "my-order-detail" */ './pages/MyOrderDetail'),
    loading: () => null,
    modules: ['my-order-detail']
});

export default () => (
    <Fragment>
        <Header />
        <div id="content">
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path={link.PRODUCT_LIST + ':slug/:page?'} component={ProductList} />
                <Route path={link.PRODUCT_DETAIL + ':slug'} component={ProductDetail} />
                <Route path={link.SEARCH + ':page?'} component={SearchProduct} />
                <Route path={link.CART} component={Cart} />
                <Route path={link.ARTICLE_LIST + ':page?'} component={ArticleList} />
                <Route path={link.ARTICLE_DETAIL + ':slug'} component={ArticleDetail} />
                <UnauthenticatedRoute path={link.LOGIN} component={Login} />
                <UnauthenticatedRoute path={link.REGISTER} component={Register} />
                <UnauthenticatedRoute path={link.RESET_PASSWORD_REQUEST} component={ResetPasswordRequest} />
                <UnauthenticatedRoute path={link.RESET_PASSWORD_CONFIRM + ':token'} component={ResetPasswordConfirm} />
                <Route path={link.ACCOUNT} component={Account} />
                <Route path={link.CHECKOUT} component={Checkout} />
                <Route path={link.MY_ORDER} component={MyOrder} />
                <Route path={link.MY_ORDER_DETAIL + ':code'} component={MyOrderDetail} />
                <Route component={NotFound} />
            </Switch>
        </div>
        <Footer />
    </Fragment>

);
