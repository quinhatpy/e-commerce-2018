import React, { Component } from 'react';
import LoadingBar from 'react-redux-loading-bar';
import { withRouter } from 'react-router';
import Routes from './routes';

class App extends Component {
    render() {
        return (
            <div className="wrap">
                <LoadingBar style={{ backgroundColor: '#f44d60 ', height: '5px', zIndex: 99999999, position: 'fixed' }} />
                <Routes />
                <a href="/" className="radius scroll-top style1"><i className="fa fa-angle-up" aria-hidden="true" /> </a>
            </div>
        );
    }
}

export default withRouter(App);
