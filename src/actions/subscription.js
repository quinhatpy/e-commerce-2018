import { hideLoading, showLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';

export const subscriptionRequest = data => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.post('subscriptions', data);
        if ('serviceWorker' in navigator) {
            var options = {
              body: 'Bạn đã đăng ký nhận thông báo thành công!',
              icon: 'https://e2018-nit.herokuapp.com/icons/android-icon-96x96.png',
              badge: 'https://e2018-nit.herokuapp.com/icons/android-icon-96x96.png',
              tag: 'confirm-notification',
            };
        
            navigator.serviceWorker.ready
              .then(function(swreg) {
                swreg.showNotification('E2018 - ' + response.data.message, options);
              });
          }
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}