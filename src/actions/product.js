import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_BEST_SELLING, FETCH_HOT_PRODUCTS, FETCH_PRODUCT, FETCH_PRODUCTS, FETCH_PRODUCT_BOXES, FETCH_SIMILAR_PRODUCTS, FETCH_SUPER_DEALS } from '../constants/actionType';
import api from '../utils/axios';

export const fetchSuperDealsRequest = (limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-super-deals?limit=${limit}`);
        dispatch(fetchSuperDeals(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchHotProductsRequest = (limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-hot-products?limit=${limit}`);
        dispatch(fetchHotProducts(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchProductBoxesRequest = (limit, categoryIdArray) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-product-boxes?limit=${limit}&parent_categories=${categoryIdArray.join(',')}`);
        dispatch(fetchProductBoxes({ data: response.data.result }));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchBestSellingByCategoryRequest = (slug, limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-best-selling?limit=${limit}&slug=${slug}`);
        dispatch(fetchBestSelling(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchProductsRequest = (slug, offset, limit, params = {}) => async dispatch => {
    try {
        dispatch(showLoading());
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        paramsClone.slug = slug;
        let queryParams = queryString.stringify(paramsClone);
        const response = await api.get(`get-products?${queryParams}`);
        dispatch(fetchProducts(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchSimilarProductsRequest = (slug, limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-similar-products?limit=${limit}&slug=${slug}`);
        dispatch(fetchSimilarProducts(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchBestSellingProductsRequest = (slug, limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-best-selling-products?limit=${limit}&slug=${slug}`);
        dispatch(fetchBestSelling(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchProductRequest = slug => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-product?slug=${slug}`);
        dispatch(fetchProduct(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchSearchProductsRequest = (offset, limit, params = {}) => async dispatch => {
    try {
        dispatch(showLoading());
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        const response = await api.get(`search-products?${queryParams}`);
        dispatch(fetchProducts(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchSuperDeals = (payload) => ({ type: FETCH_SUPER_DEALS, payload });
const fetchHotProducts = (payload) => ({ type: FETCH_HOT_PRODUCTS, payload });
const fetchProductBoxes = (payload) => ({ type: FETCH_PRODUCT_BOXES, payload });
const fetchBestSelling = (payload) => ({ type: FETCH_BEST_SELLING, payload });
const fetchProducts = (payload) => ({ type: FETCH_PRODUCTS, payload });
const fetchSimilarProducts = (payload) => ({ type: FETCH_SIMILAR_PRODUCTS, payload });
const fetchProduct = (payload) => ({ type: FETCH_PRODUCT, payload });