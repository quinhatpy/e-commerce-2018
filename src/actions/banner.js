import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_BANNERS } from '../constants/actionType';
import api from '../utils/axios';

export const fetchBannersRequest = (position, limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-banners?position=${position}&limit=${limit}`);
        dispatch(fetchCategories(response.data.result, position));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchCategories = (payload, position) => ({ type: FETCH_BANNERS, payload, position });