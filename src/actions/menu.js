import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_MENUS } from '../constants/actionType';
import api from '../utils/axios';

export const fetchMenusRequest = () => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get('get-menus');
        dispatch(fetchMenus(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchMenus = (payload) => ({ type: FETCH_MENUS, payload });