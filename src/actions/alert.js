import { SET_ALERT } from './../constants/actionType';

export const setAlert = (typeAlert, title, content = {}) => ({ type: SET_ALERT, typeAlert, title, content });
export const resetAlert = () => ({ type: SET_ALERT, typeAlert: '', title: '', content: {} });