import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_CATEGORIES, FETCH_CATEGORY } from '../constants/actionType';
import api from '../utils/axios';

export const fetchCategoriesRequest = () => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get('get-categories');
        dispatch(fetchCategories(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchCategoryRequest = slug => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-category?slug=${slug}`);
        dispatch(fetchCategory(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchCategories = (payload) => ({ type: FETCH_CATEGORIES, payload });
const fetchCategory = (payload) => ({ type: FETCH_CATEGORY, payload });