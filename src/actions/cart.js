import { ADD_TO_CART, INIT_CART, REMOVE_CART_ITEM, UPDATE_CART_ITEM } from '../constants/actionType';

export const initCart = () => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    
    return { type: INIT_CART, payload: cart }
}

export const resetCart = () => {
    return { type: INIT_CART, payload: [] }
}

export const addToCart = payload => ({ type: ADD_TO_CART, payload });
export const updateCartItem = (index, quantity) => ({ type: UPDATE_CART_ITEM, payload: { index, quantity } });
export const removeCartItem = payload => ({ type: REMOVE_CART_ITEM, payload });