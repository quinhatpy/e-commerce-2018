import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_MY_ORDERS, FETCH_MY_ORDER_DETAIL } from '../constants/actionType';
import api from '../utils/axios';

export const fetchMyOrderRequest = () => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get('my-orders');
        dispatch(fetchMyOrder(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchMyOrderDetailRequest = (code) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`my-order-detail?code=${code}`);
        dispatch(fetchMyOrderDetail(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const cancelOrderRequest = (code) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`cancel-order?code=${code}`);
        dispatch(fetchMyOrderDetail(response.data.result));
        dispatch(fetchMyOrderRequest());
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchMyOrder = (payload) => ({ type: FETCH_MY_ORDERS, payload });
const fetchMyOrderDetail = (payload) => ({ type: FETCH_MY_ORDER_DETAIL, payload });