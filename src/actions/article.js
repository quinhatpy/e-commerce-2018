import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_ARTICLE, FETCH_ARTICLES } from '../constants/actionType';
import api from '../utils/axios';

export const fetchArticlesRequest = (offset, limit) => async dispatch => {
    try {
        dispatch(showLoading());
        let paramsClone = { offset, limit };
        let queryParams = queryString.stringify(paramsClone);

        const response = await api.get(`get-articles?${queryParams}`);

        dispatch(fetchArticles(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchArticleRequest = slug => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-article?slug=${slug}`);
        dispatch(fetchArticle(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchArticles = (payload) => ({ type: FETCH_ARTICLES, payload });
const fetchArticle = (payload) => ({ type: FETCH_ARTICLE, payload });