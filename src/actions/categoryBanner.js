import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_CATEGORY_BANNERS_IN_HOME, FETCH_CATEGORY_BANNERS_IN_PRODUCT_LIST } from '../constants/actionType';
import api from '../utils/axios';

export const fetchCategoryBannerInHomeRequest = categoryIdArray => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-home-category-banner?parent_categories=${categoryIdArray.join(',')}`);
        dispatch(fetchCategoryBannerInHome({ data: response.data.result }));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchCategoryBannerInProductListRequest = (slug, limit) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-category-banners?slug=${slug}&limit=${limit}`);
        dispatch(fetchCategoryBannerInProductList(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchCategoryBannerInHome = (payload) => ({ type: FETCH_CATEGORY_BANNERS_IN_HOME, payload });
const fetchCategoryBannerInProductList = (payload) => ({ type: FETCH_CATEGORY_BANNERS_IN_PRODUCT_LIST, payload });