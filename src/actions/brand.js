import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_BRANDS_IN_HOME } from '../constants/actionType';
import api from '../utils/axios';

export const fetchBrandsInHomeRequest = limit => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-home-brands?limit=${limit}`);
        dispatch(fetchBrandsInHome(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

const fetchBrandsInHome = payload => ({ type: FETCH_BRANDS_IN_HOME, payload });