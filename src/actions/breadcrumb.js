import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { FETCH_BREADCRUMB } from '../constants/actionType';
import api from '../utils/axios';

export const fetchBreadcrumbProductListRequest = slug => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-breadcrumb-product-list?slug=${slug}`);
        dispatch(fetchBreadcrumb(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const fetchBreadcrumbProductDetailRequest = slug => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get(`get-breadcrumb-product-detail?slug=${slug}`);
        dispatch(fetchBreadcrumb(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}


const fetchBreadcrumb = (payload) => ({ type: FETCH_BREADCRUMB, payload });