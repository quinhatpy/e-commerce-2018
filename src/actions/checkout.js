import { hideLoading, showLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';
import { resetAlert, setAlert } from './alert';
import { resetCart } from './cart';

export const checkoutRequest = (data, history) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.post('place-order', data);
        localStorage.removeItem('cart');
        dispatch(resetCart());
        alert(response.data.message);
        history.push({
            pathname: '/'
        });
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}