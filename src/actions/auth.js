import Cookies from 'js-cookie';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { LOGOUT, SET_INFO_RESET_PASSWORD, SET_USER_CURRENT } from '../constants/actionType';
import api from '../utils/axios';
import { resetAlert, setAlert } from './alert';

export const loginRequest = (data) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.post('login', data);
        const result = response.data.result;

        api.defaults.headers.common['Authorization'] = `Bearer ${result.token}`;
        Cookies.set('accessToken', result.token);

        const dataResponse = await api.get('user');
        dispatch(setUserCurrent(dataResponse.data.result));
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}

export const fetchUserCurrentRequest = () => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get('user');
        // console.log('TEST AUTH1', response.data.result);
        dispatch(setUserCurrent(response.data.result));
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}


export const logoutRequest = () => async dispatch => {
    try {
        dispatch(showLoading());
        await api.get('logout');
        dispatch(logout());
        dispatch(hideLoading());
    } catch (error) {
        console.error(error);
        dispatch(hideLoading());
    }
}

export const updateCustomerRequest = (data) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.post('update-customer', data);
        dispatch(setUserCurrent(response.data.result));
        alert('Cập nhật thành công!');
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}

export const registerRequest = (data, history) => async dispatch => {
    try {
        dispatch(showLoading());
        await api.post('register', data);
        window.alert('Đăng ký thành công!');
        history.push({
            pathname: 'dang-nhap'
        });
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}

export const createResetPasswordRequest = (data, history) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.post('password/create', data);
        window.alert(response.data.message);
        history.push({
            pathname: '/'
        });
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}

export const findResetPasswordRequest = (token) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.get('password/find/'+ token);
        dispatch(setInfoResetPassword(response.data.result));
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}

export const resetPasswordRequest = (data, history) => async dispatch => {
    try {
        dispatch(showLoading());
        const response = await api.post('password/reset', data);
        window.alert(response.data.message);
        history.push({
            pathname: '/dang-nhap'
        });
        dispatch(hideLoading());
    } catch (err) {
        console.error(err.message);
        if (err.response) {
            const response = err.response.data;
            dispatch(setAlert('danger', response.message, response.data))
            setTimeout(() => dispatch(resetAlert()), 5000);
        }
        dispatch(hideLoading());
    }
}


export const setUserCurrent = (payload) => ({ type: SET_USER_CURRENT, payload });
export const setInfoResetPassword = (payload) => ({ type: SET_INFO_RESET_PASSWORD, payload });
export const logout = () => ({ type: LOGOUT });