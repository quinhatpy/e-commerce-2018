import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { registerRequest } from '../../actions';
import Alert from '../../components/Alert';
import MetaTag from '../../components/MetaTag';

class Register extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            email: '',
            password: '',
            password_confirmation: ''
        }

        this.nameRef = React.createRef();
        this.emailRef = React.createRef();
        this.passwordRef = React.createRef();
        this.passwordConfirmRef = React.createRef();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    handleSubmit(event) {
        event.preventDefault();
        if (window.navigator.onLine) {
            let isValid = true;
            const {
                name,
                email,
                password,
                password_confirmation
            } = this.state;

            isValid = this.validateInputData('name', name) && isValid;
            isValid = this.validateInputData('email', email) && isValid;
            isValid = this.validateInputData('password', password) && isValid;
            isValid = this.validateInputData('password_confirmation', password_confirmation) && isValid;

            if (isValid) {
                const {
                    history,
                    registerRequest
                } = this.props;

                let formData = new FormData(event.target);

                registerRequest(formData, history);
            }
        }
        else {
            alert('Vui lòng kết nối internet!');
        }

    }

    handleInputChange(event) {
        const target = event.target;
        this.validateInputData(target.name, target.value);

        this.setState({
            [target.name]: target.value
        });
    }

    validateInputData(input, data) {
        let valid = true;

        if (input === 'name') {
            this.nameRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.nameRef.current.innerText = 'Họ tên không được để trống.';
            }
        }

        if (input === 'email') {
            this.emailRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.emailRef.current.innerText = 'Email không được để trống.';
            }
        }

        if (input === 'password') {
            this.passwordRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.passwordRef.current.innerText = 'Mật khẩu không được để trống.';
            }
        }

        if (input === 'password_confirmation') {
            this.passwordConfirmRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.passwordConfirmRef.current.innerText = 'Mật khẩu nhập lại không được để trống.';
            }

            if (data !== this.state.password) {
                valid = false;
                this.passwordConfirmRef.current.innerText = 'Mật khẩu nhập lại không khớp.';
            }
        }

        return valid;
    }

    render() {
        const { alert } = this.props;

        return (
            <MetaTag title="Đăng ký">
                <div className="content-page woocommerce">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="register-content-box">
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-ms-12 col-sm-offset-3">
                                            <div className="check-billing">
                                                <div className="form-my-account">
                                                    <form className="block-login" onSubmit={this.handleSubmit.bind(this)}>
                                                        <h2 className="title24 title-form-account text-center">Đăng ký</h2>
                                                        <Alert type={alert.type} title={alert.title} content={alert.content} />
                                                        <p>
                                                            <label htmlFor="name">Họ và tên <span className="required">*</span></label>
                                                            <input type="text" name="name" onChange={this.handleInputChange.bind(this)} />
                                                            <span className="text-danger" ref={this.nameRef}></span>
                                                        </p>
                                                        <p>
                                                            <label htmlFor="username">Email <span className="required">*</span></label>
                                                            <input type="email" name="email" onChange={this.handleInputChange.bind(this)} />
                                                            <span className="text-danger" ref={this.emailRef}></span>
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password">Mật khẩu <span className="required">*</span></label>
                                                            <input type="password" name="password" onChange={this.handleInputChange.bind(this)} />
                                                            <span className="text-danger" ref={this.passwordRef}></span>
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password_confirmation">Nhập lại mật khẩu <span className="required">*</span></label>
                                                            <input type="password" name="password_confirmation" onChange={this.handleInputChange.bind(this)} />
                                                            <span className="text-danger" ref={this.passwordConfirmRef}></span>
                                                        </p>
                                                        <p>
                                                            <input type="submit" className="register-button" name="login" value="Đăng ký" />
                                                        </p>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

Register.propTypes = {
    alert: PropTypes.object,
    registerRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    alert: state.alert
})

const mapDispatchToProps = dispatch => ({
    registerRequest: (data, history) => dispatch(registerRequest(data, history))
})

export default connect(mapStateToProps, mapDispatchToProps)(Register);
