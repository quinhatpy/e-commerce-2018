import PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { fetchArticlesRequest, fetchSuperDealsRequest } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import ContentBlog from '../../components/ContentBlog';
import MetaTag from '../../components/MetaTag';
import SuperDeal from '../../components/SuperDeal';
import * as link from '../../constants/link';
import scrollTop from '../../utils/scrollTop';

const productPerPage = 5;

const frontLoad = async props => {
    await props.fetchSuperDealsRequest(10);

    let offset = 0;
    if (props.match.params.page) {
        offset = (parseInt(props.match.params.page) - 1) * productPerPage;
    }

    await props.fetchArticlesRequest(offset, productPerPage);
}

class ArticleList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            currentPage: 1,
            offset: 0,
            params: {},
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.setState({ currentPage: parseInt(this.props.match.params.page) || 1 })
    }

    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * productPerPage;
        stateClone.offset = offset;
        this.setState(stateClone);

        this.props.fetchArticlesRequest(offset, productPerPage);
        scrollTop();
    }

    render() {
        const {
            currentPage,
            offset
        } = this.state;

        const {
            superDeals,
            articles,
            totalRows
        } = this.props;

        return (
            <MetaTag title="Tin tức">
                <div className="content-page">
                    <div className="container">
                        <Breadcrumb breadcrumb={[{ name: 'Tin tức' }]} />
                        <div className="row">
                            <div className="col-md-9 col-sm-8 col-xs-12 pull-right">
                                <div className="content-blog-page border radius">
                                    <ContentBlog
                                        articles={articles}
                                        totalRecord={totalRows}
                                        limit={productPerPage}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={link.ARTICLE_LIST}
                                        onPageChange={this.handlePageChange.bind(this)}
                                    />
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-4 col-xs-12">
                                <div className="sidebar sidebar-left">
                                    <SuperDeal superDeals={superDeals} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </MetaTag>
        )
    }
}

ArticleList.propTypes = {
    superDeals: PropTypes.array.isRequired,
    articles: PropTypes.array.isRequired,
    totalRows: PropTypes.number.isRequired,
    fetchSuperDealsRequest: PropTypes.func.isRequired,
    fetchArticlesRequest: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    superDeals: state.product.superDeals,
    articles: state.article.list,
    totalRows: state.article.totalRows
});

const mapDispatchToProps = dispatch => ({
    fetchSuperDealsRequest: limit => dispatch(fetchSuperDealsRequest(limit)),
    fetchArticlesRequest: (offset, limit) => dispatch(fetchArticlesRequest(offset, limit)),
})

export default connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(ArticleList)
);
