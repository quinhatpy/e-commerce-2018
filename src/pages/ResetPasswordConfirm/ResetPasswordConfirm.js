import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { findResetPasswordRequest, resetPasswordRequest } from '../../actions';
import Alert from '../../components/Alert';
import MetaTag from '../../components/MetaTag';

class ResetPasswordConfirm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            token: ''
        }
    }

    componentDidMount() {
        const {
            match,
            findResetPasswordRequest
        } = this.props;

        window.scrollTo(0, 0);
        findResetPasswordRequest(match.params.token)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            email: nextProps.infoResetPassword.email,
            token: nextProps.infoResetPassword.token,
        })
    }

    handleSubmit(event) {
        const {
            history,
            resetPasswordRequest
        } = this.props;

        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('email', this.state.email);
        formData.set('token', this.state.token);

       resetPasswordRequest(formData, history);
    }

    render() {
        const { email } = this.state;
        const { alert } = this.props;

        return (
            <MetaTag title="Cập nhật mật khẩu">
                <div className="content-page woocommerce">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="register-content-box">
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-ms-12 col-sm-offset-3">
                                            <div className="check-billing">
                                                <div className="form-my-account">
                                                    <form className="block-login" onSubmit={this.handleSubmit.bind(this)}>
                                                        <h2 className="title24 title-form-account text-center">Cập nhật mật khẩu</h2>
                                                        <Alert type={alert.type} title={alert.title} content={alert.content} />
                                                        <p>
                                                            <label htmlFor="username">Email <span className="required">*</span></label>
                                                            <input type="email" name="email" disabled defaultValue={email} />
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password">Mật khẩu mới <span className="required">*</span></label>
                                                            <input type="password" name="password" />
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password_confirmation">Nhập lại mật khẩu <span className="required">*</span></label>
                                                            <input type="password" name="password_confirmation" />
                                                        </p>
                                                        <p>
                                                            <input type="submit" className="register-button" name="login" value="Lấy lại mật khẩu" />
                                                        </p>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

ResetPasswordConfirm.propTypes = {
    alert: PropTypes.object,
    infoResetPassword: PropTypes.shape({
        id: PropTypes.number.isRequired,
        email: PropTypes.string.isRequired,
        token: PropTypes.string.isRequired,
    }).isRequired,
    findResetPasswordRequest: PropTypes.func.isRequired,
    updateCustomerRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    alert: state.alert,
    infoResetPassword: state.auth.infoResetPassword
})

const mapDispatchToProps = dispatch => ({
    findResetPasswordRequest: data => dispatch(findResetPasswordRequest(data)),
    resetPasswordRequest: (data, history) => dispatch(resetPasswordRequest(data, history))
})

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordConfirm);
