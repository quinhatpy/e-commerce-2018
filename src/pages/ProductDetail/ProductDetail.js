import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { addToCart, fetchBestSellingProductsRequest, fetchBreadcrumbProductDetailRequest, fetchProductRequest, fetchSimilarProductsRequest } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import GalleryThumbnail from '../../components/GalleryThumbnail';
import MetaTag from '../../components/MetaTag';
import ProductBranch from '../../components/ProductBranch';
import ProductRelated from '../../components/ProductRelated';
import ProductSelling from '../../components/ProductSelling';
import * as link from '../../constants/link';
import { formatMoney, getPageMeta } from '../../utils/helper';

const frontLoad = async props => {
    await props.fetchBreadcrumbProductDetailRequest(props.match.params.slug);
    await props.fetchSimilarProductsRequest(props.match.params.slug, 8);
    await props.fetchBestSellingProductsRequest(props.match.params.slug, 10);
    await props.fetchProductRequest(props.match.params.slug);
}

class ProductDetail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            quantity: 1
        }
    }

    componentDidUpdate() {
        window.scrollTo(0, 0);
    }

    componentWillReceiveProps(nextProps) {
        const {
            match,
            fetchBreadcrumbProductDetailRequest,
            fetchProductRequest
        } = this.props;

        if (nextProps.match.params.slug !== match.params.slug) {
            fetchBreadcrumbProductDetailRequest(nextProps.match.params.slug);
            fetchProductRequest(nextProps.match.params.slug);
        }
    }

    renderAttributeItem(attributes) {
        if (attributes)
            return attributes.map(attribute => {
                if (attribute.content)
                    return (
                        <tr key={attribute.id}>
                            <td>{attribute.name}</td>
                            <td>{attribute.content}</td>
                        </tr>
                    )
                return null;
            });
    }

    handleChangeQuantity(event) {
        let quantity = parseInt(event.target.value);
        if (isNaN(quantity) || quantity < 1) quantity = 1;
        this.setState({ quantity })
    }

    handleAddToCart(product) {
        const {
            history,
            addToCart
        } = this.props;
        const cartItem = {
            id: product.id,
            name: product.name,
            thumbnail: product.thumbnail,
            slug: product.slug,
            price: product.promotion ? product.promotion.new_price : product.price,
            quantity: this.state.quantity
        }

        addToCart(cartItem);
        history.push({
            pathname: link.CART,
        });
    }

    render() {
        const { quantity } = this.state;
        const {
            breadcrumb,
            similarProducts,
            bestSelling,
            product
        } = this.props;
        const pageMeta = getPageMeta(breadcrumb)

        return (
            <MetaTag {...pageMeta}>
                <div className="content-page">
                    <div className="container">
                        <Breadcrumb breadcrumb={breadcrumb} />
                        <div className="row">
                            <div className="col-md-9 col-sm-8 col-col-xs-12">
                                <div className="product-detail accordion-detail border radius">
                                    <div className="row">
                                        <div className="col-md-5 col-sm-12 col-xs-12">
                                            <GalleryThumbnail images={product.images} />
                                        </div>
                                        <div className="col-md-7 col-sm-12 col-xs-12">
                                            <div className="detail-info">
                                                <h2 className="title-detail">{product.name}</h2>
                                                <div className="product-rate">
                                                    <div className="product-rating" style={{ width: `${product.rating === null ? 100 : ((product.rating * 100) / 5)}%` }} />
                                                </div>
                                                <p className="desc" dangerouslySetInnerHTML={{ __html: product.description }}></p>
                                                <div className="product-price">
                                                    {
                                                        product.promotion ?
                                                            (
                                                                <Fragment>
                                                                    <del><span>{formatMoney(product.price)}</span></del>
                                                                    <ins><span>{formatMoney(product.promotion.new_price)}</span></ins>
                                                                </Fragment>
                                                            )
                                                            :
                                                            (
                                                                <ins><span>{formatMoney(product.price)}</span></ins>
                                                            )
                                                    }
                                                </div>
                                                {
                                                    product.quantities && product.quantities.length !== 0 ?
                                                        (
                                                            <div className="detail-extralink">
                                                                <div className="detail-qty border radius">
                                                                    <input type="number" defaultValue={quantity} onChange={this.handleChangeQuantity.bind(this)} min="1" />
                                                                </div>
                                                                <div className="product-extra-link2">
                                                                    <span className="addcart-link link" onClick={this.handleAddToCart.bind(this, product)}>Thêm vào giỏ hàng</span>
                                                                </div>
                                                            </div>
                                                        )
                                                        : null
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-detal toggle-tab">
                                        <h2>Thông số kỹ thuật</h2>
                                        <table className="table table-bordered table-hover">
                                            <tbody>
                                                {this.renderAttributeItem(product.attributes)}
                                            </tbody>
                                        </table>
                                        <h2>Mô tả</h2>
                                        <div dangerouslySetInnerHTML={{ __html: product.content }}></div>
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-3 col-sm-4 col-col-xs-12">
                                <div className="sidebar sidebar-right">
                                    <ProductBranch branches={product.quantities} />
                                    <ProductSelling bestSelling={bestSelling} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <ProductRelated similarProducts={similarProducts} />
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

ProductDetail.propTypes = {
    breadcrumb: PropTypes.array.isRequired,
    similarProducts: PropTypes.array.isRequired,
    bestSelling: PropTypes.array.isRequired,
    product: PropTypes.shape({
        id: PropTypes.number,
        code: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.number,
        receiver_address: PropTypes.string,
        thumbnail: PropTypes.string,
        description: PropTypes.string,
        content: PropTypes.string,
        view: PropTypes.number,
        slug: PropTypes.string,
        rating: PropTypes.number,
        attributes: PropTypes.array,
        images: PropTypes.array,
        quantities: PropTypes.array,
        promotion: PropTypes.shape({
            new_price: PropTypes.number
        }),
    }).isRequired,
    fetchBreadcrumbProductDetailRequest: PropTypes.func.isRequired,
    fetchSimilarProductsRequest: PropTypes.func.isRequired,
    fetchBestSellingProductsRequest: PropTypes.func.isRequired,
    fetchProductRequest: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    breadcrumb: state.breadcrumb,
    similarProducts: state.product.similar,
    bestSelling: state.product.bestSelling,
    product: state.product.item
});

const mapDispatchToProps = dispatch => ({
    fetchBreadcrumbProductDetailRequest: slug => dispatch(fetchBreadcrumbProductDetailRequest(slug)),
    fetchSimilarProductsRequest: (slug, limit) => dispatch(fetchSimilarProductsRequest(slug, limit)),
    fetchBestSellingProductsRequest: (slug, limit) => dispatch(fetchBestSellingProductsRequest(slug, limit)),
    fetchProductRequest: slug => dispatch(fetchProductRequest(slug)),
    addToCart: product => dispatch(addToCart(product))
})

export default connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(ProductDetail)
);
