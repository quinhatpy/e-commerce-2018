import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { checkoutRequest, resetCart } from '../../actions';
import Alert from '../../components/Alert';
import MetaTag from '../../components/MetaTag';
import Breadcrumb from '../../components/Breadcrumb';
import CartItemBasic from '../../components/CartItemBasic';
import { formatMoney } from '../../utils/helper';

class Checkout extends Component {
    constructor(props) {
        super(props)

        this.state = {
            receiver_name: '',
            receiver_phone: '',
            receiver_address: ''
        }

        this.nameRef = React.createRef();
        this.phoneRef = React.createRef();
        this.addressRef = React.createRef();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        const user = this.props.user;

        if (!isEmpty(user)) {
            this.setState({
                receiver_name: user.name,
                receiver_phone: user.phone,
                receiver_address: user.address
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const user = nextProps.user;

        if (!isEmpty(user)) {
            this.setState({
                receiver_name: user.name,
                receiver_phone: user.phone,
                receiver_address: user.address
            });
        }
    }

    renderCartItem(cart) {
        return cart.map((item, index) => (
            <CartItemBasic
                key={item.id}
                item={item}
                index={index + 1} />
        ))
    }

    countTotal(cart) {
        return cart.reduce((total, item) => total += item.price * item.quantity, 0)
    }

    handleEnableNotification() {
        if ('Notification' in window && 'serviceWorker' in navigator) {
            Notification.requestPermission(function (result) {
                console.log('User Choice', result);
                if (result !== 'granted') {
                    console.log('No notification permission granted!');
                } else {
                    if ('serviceWorker' in navigator) {
                        var options = {
                            body: 'Bật thông báo trạng thái đặt hàng thành công!',
                            icon: 'https://e2018-nit.herokuapp.com/icons/android-icon-96x96.png',
                            badge: 'https://e2018-nit.herokuapp.com/icons/android-icon-96x96.png',
                        };

                        navigator.serviceWorker.ready
                            .then(function (swreg) {
                                swreg.showNotification('E-Commerce 2018 - Thông báo', options);
                            });
                    }
                }
            });
        }
    }

    handleInputChange(event) {
        const target = event.target;
        this.validateInputData(target.name, target.value);

        this.setState({
            [target.name]: target.value
        });
    }

    validateInputData(input, data) {
        let valid = true;

        if (input === 'receiver_name') {
            this.nameRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.nameRef.current.innerText = 'Họ tên không được để trống.';
            }
        }

        if (input === 'receiver_phone') {
            this.phoneRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.phoneRef.current.innerText = 'Số điện thoại không được để trống.';
            }
        }

        if (input === 'receiver_address') {
            this.addressRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.addressRef.current.innerText = 'Địa chỉ không được để trống.';
            }
        }

        return valid;
    }

    handleCheckout(event) {
        event.preventDefault();
        let isValid = true;
        const {
            receiver_name,
            receiver_phone,
            receiver_address,
        } = this.state;

        isValid = this.validateInputData('receiver_name', receiver_name) && isValid;
        isValid = this.validateInputData('receiver_phone', receiver_phone) && isValid;
        isValid = this.validateInputData('receiver_address', receiver_address) && isValid;

        if (isValid) {
            let formData = new FormData(event.target);
            const {
                cart,
                user,
                history,
                checkoutRequest
            } = this.props;

            let formDataObject = {};
            for (var pair of formData.entries()) {
                // console.log(pair[0] + ', ' + pair[1]);
                formDataObject[pair[0]] = pair[1]
            }

            formDataObject.carts = {};

            cart.forEach(item => {
                formDataObject.carts[item.id] = item.quantity;
            });

            cart.forEach(item => {
                formData.set(`carts[${item.id}]`, item.quantity);
            });

            if (!isEmpty(user))
                formData.set('customer_id', user.id)

            console.log('[NETWORK CONNECTED]', window.navigator.onLine);
            if (window.navigator.onLine) {
                checkoutRequest(formData, history);
            }
            else {
                this.handleEnableNotification();

                if ('serviceWorker' in navigator && 'SyncManager' in window) {
                    console.log(navigator.serviceWorker);
                    navigator.serviceWorker.ready
                        .then(function (sw) {

                            let indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
                            if (indexedDB) {
                                var request = indexedDB.open('ecommerce2018', 1);

                                request.onupgradeneeded = function (event) {
                                    var db = event.target.result;
                                    // console.log('[CHECK TABLE EXIST]', db.objectStoreNames.contains('sync-checkout'));
                                    db.createObjectStore("sync-checkout", { keyPath: "id" });
                                }
                                request.onsuccess = function (event) {
                                    var db = event.target.result;
                                    var tx = db.transaction('sync-checkout', 'readwrite');
                                    var store = tx.objectStore('sync-checkout');
                                    let checkoutData = {
                                        id: new Date().getTime(),
                                        data: formDataObject
                                    }

                                    let result = store.put(checkoutData);
                                    result.onsuccess = function (event) {
                                        sw.sync.register('sync-new-checkout');

                                        alert('Đặt hàng thành công, Vui lòng kết nối internet để đồng bộ!');
                                    };
                                };
                            }
                            else console.log('IndexedDB is not supported');

                        })
                        .catch(err => {
                            console.log('[ERROR]', err);
                        });
                }
            }
        }
    }

    render() {
        const breadcrumb = [{ name: 'Đặt hàng' }];
        const {
            cart,
            alert
        } = this.props;
        const {
            receiver_name,
            receiver_phone,
            receiver_address
        } = this.state;

        return (
            <MetaTag title="Đặt hàng">
                <div className="content-page woocommerce">
                    <div className="container register-content-box">
                        <Breadcrumb breadcrumb={breadcrumb} />
                        <h2 className="title-shop-page">Đặt hàng</h2>
                        <div className="form-my-account col-md-6">
                            <form method="post" className="block-login" onSubmit={this.handleCheckout.bind(this)}>
                                <h2 className="title18 text-center">Thông tin đặt hàng</h2>
                                <Alert type={alert.type} title={alert.title} content={alert.content} />
                                <p>
                                    <label htmlFor="receiver_name">Tên người nhận <span className="required">*</span></label>
                                    <input type="text" name="receiver_name" value={receiver_name} onChange={this.handleInputChange.bind(this)} />
                                    <span className="text-danger" ref={this.nameRef}></span>
                                </p>
                                <p>
                                    <label htmlFor="receiver_phone">Số điện thoại người nhận <span className="required">*</span></label>
                                    <input type="text" name="receiver_phone" defaultValue={receiver_phone} onChange={this.handleInputChange.bind(this)} />
                                    <span className="text-danger" ref={this.phoneRef}></span>
                                </p>
                                <p>
                                    <label htmlFor="receiver_address">Địa chỉ người nhận <span className="required">*</span></label>
                                    <textarea name="receiver_address" defaultValue={receiver_address} onChange={this.handleInputChange.bind(this)}></textarea>
                                    <span className="text-danger" ref={this.addressRef}></span>
                                </p>
                                <p>
                                    <label htmlFor="note">Ghi chú</label>
                                    <textarea name="note"></textarea>
                                </p>
                                {cart.length > 0 ?
                                    <p>
                                        <input type="submit" className="register-button" name="login" value="Đặt hàng" />
                                    </p> : null}
                            </form>
                        </div>
                        <div className="cart-content-page col-md-6">
                            <h2 className="title18 text-center">Các sản phẩm đã chọn</h2>
                            <br />
                            <div className="table-responsive">
                                <table cellSpacing={0} className="shop_table cart table">
                                    <thead>
                                        <tr>
                                            <th className="product-remove">#</th>
                                            <th className="product-thumbnail">Hình</th>
                                            <th className="product-name">Tên sản phẩm</th>
                                            <th className="product-price">Giá</th>
                                            <th className="product-quantity">Số lượng</th>
                                            <th className="product-subtotal">Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderCartItem(cart)}
                                        {
                                            cart.length > 0 ?
                                                <tr>
                                                    <td className="actions" colSpan={7}>
                                                        <label htmlFor="coupon_code">Tổng cộng: </label>
                                                        <span>{formatMoney(this.countTotal(cart))}<sup>đ</sup></span>
                                                    </td>
                                                </tr>
                                                : null
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag >
        )
    }
}

Checkout.propTypes = {
    alert: PropTypes.object,
    user: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        email: PropTypes.string,
        address: PropTypes.string,
        phone: PropTypes.string
    }),
    cart: PropTypes.array.isRequired,
    checkoutRequest: PropTypes.func.isRequired,
    resetCart: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    cart: state.cart,
    user: state.auth.user,
    alert: state.alert
});

const mapDispatchToProps = dispatch => ({
    checkoutRequest: (data, history) => dispatch(checkoutRequest(data, history)),
    resetCart: () => dispatch(resetCart())
})

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
