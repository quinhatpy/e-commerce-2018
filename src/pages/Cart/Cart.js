import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { removeCartItem, updateCartItem } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import CartItem from '../../components/CartItem';
import MetaTag from '../../components/MetaTag';
import * as link from '../../constants/link';
import { formatMoney } from '../../utils/helper';

class Cart extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    renderCartItem(cart) {
        if (cart.length === 0)
            return (
                <tr className="cart_item">
                    <td className="text-center" colSpan={7}>
                        Không có sản phẩm nào trong giỏ hàng
                    </td>
                </tr>
            )
        return cart.map((item, index) => (
            <CartItem
                key={item.id}
                item={item}
                index={index + 1}
                onRemoveCartItem={this.handleRemoveCartItem.bind(this, index)}
                onUpdateCartItem={this.handleUpdateCartItem.bind(this, index)} />
        ))
    }

    countTotal(cart) {
        return cart.reduce((total, item) => total += item.price * item.quantity, 0)
    }

    handleRemoveCartItem(index) {
        if (window.confirm('Bạn có muốn xóa sản phẩm này ra khỏi giỏ hàng!'))
            this.props.removeCartItem(index);
    }

    handleUpdateCartItem(index, event) {
        const quantity = isNaN(parseInt(event.target.value)) ? 1 : parseInt(event.target.value);
        this.props.updateCartItem(index, quantity);
    }

    render() {
        const breadcrumb = [{ name: 'Giỏ hàng' }];
        const { cart } = this.props;

        return (
            <MetaTag title="Giỏ hàng">
                <div className="content-page woocommerce">
                    <div className="container">
                        <Breadcrumb breadcrumb={breadcrumb} />
                        <div className="cart-content-page">
                            <h2 className="title-shop-page">Giỏ hàng</h2>
                            <form method="post">
                                <div className="table-responsive">
                                    <table cellSpacing={0} className="shop_table cart table">
                                        <thead>
                                            <tr>
                                                <th className="product-remove">#</th>
                                                <th className="product-thumbnail">Hình sản phẩm</th>
                                                <th className="product-name">Tên sản phẩm</th>
                                                <th className="product-price">Giá</th>
                                                <th className="product-quantity">Số lượng</th>
                                                <th className="product-subtotal">Thành tiền</th>
                                                <th className="product-remove"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.renderCartItem(cart)}
                                            {
                                                cart.length > 0 ?
                                                    <Fragment>
                                                        <tr>
                                                            <td className="actions" colSpan={7}>
                                                                <label htmlFor="coupon_code">Tổng cộng: </label>
                                                                <span>{formatMoney(this.countTotal(cart))}<sup>đ</sup></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="actions" colSpan={7}>
                                                                <div className="wc-proceed-to-checkout">
                                                                    <Link className="checkout-button button alt wc-forward" to={link.CHECKOUT}>Đặt hàng</Link>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </Fragment>
                                                    : null
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

Cart.propTypes = {
    cart: PropTypes.array.isRequired,
    updateCartItem: PropTypes.func.isRequired,
    removeCartItem: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    cart: state.cart
});

const mapDispatchToProps = dispatch => ({
    updateCartItem: (index, quantity) => dispatch(updateCartItem(index, quantity)),
    removeCartItem: index => dispatch(removeCartItem(index))
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
