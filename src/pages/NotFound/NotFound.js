import React, { Component } from 'react';
import image404 from '../../assets/404.png';
import MetaTag from '../../components/MetaTag';

export default class NotFound extends Component {
    render() {
        return (
            <MetaTag title="Trang không tồn tại">
                <div className="content-page woocommerce">

                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 text-center">
                                <img src={image404} alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}
