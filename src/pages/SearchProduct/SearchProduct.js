import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { addToCart, fetchHotProductsRequest, fetchSearchProductsRequest } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import FilterBar from '../../components/FilterBar';
import ListProduct from '../../components/ListProduct';
import MetaTag from '../../components/MetaTag/MetaTag';
import ProductBestSelling from '../../components/ProductSelling';
import * as link from '../../constants/link';
import scrollTop from '../../utils/scrollTop';

const productPerPage = 5;

const frontLoad = async props => {
    if (isEmpty(props.hotProducts))
        await props.fetchHotProductsRequest(10);

    const queryParams = queryString.parse(props.location.search);
    let offset = 0;
    if (props.match.params.page) {
        offset = (parseInt(props.match.params.page) - 1) * productPerPage;
    }
    await props.fetchSearchProductsRequest(offset, productPerPage, queryParams);
}

class SearchProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            currentPage: 1,
            offset: 0,
            params: {},
        }
    }

    componentDidMount() {
        const {
            location,
            match
        } = this.props;

        window.scrollTo(0, 0);
        const queryParams = queryString.parse(location.search);

        this.setState({ currentPage: parseInt(match.params.page) || 1, params: queryParams })
    }

    componentWillReceiveProps(nextProps) {
        const queryParams = queryString.parse(nextProps.location.search);

        if (parseInt(queryParams.category_id) !== parseInt(this.state.params.category_id) || queryParams.keyword !== this.state.params.keyword) {
            this.handlePageChange(1, nextProps.location.search);
        }
    }


    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);
        let offset = (pageNumber - 1) * productPerPage;
        stateClone.offset = offset;
        this.setState(stateClone);

        this.props.fetchSearchProductsRequest(offset, productPerPage, stateClone.params);
        scrollTop();
    }

    handleSort(event) {
        const sortBy = event.target.value;

        let stateClone = { ...this.state };
        stateClone.params.sort_by = sortBy;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: link.SEARCH,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleAddToCart(cartItem) {
        const {
            history,
            addToCart
        } = this.props;

        addToCart(cartItem);
        history.push({
            pathname: link.CART,
        });
    }

    render() {
        const {
            params,
            currentPage,
            offset
        } = this.state;
        const {
            hotProducts,
            products,
            totalRows,
        } = this.props;

        const breadcrumb = [{ name: 'Tìm kiếm sản phẩm: ' + params.keyword }];
        return (
            <MetaTag title="Tìm kiếm">
                <div className="content-page">
                    <div className="container">
                        <Breadcrumb breadcrumb={breadcrumb} />

                        <div className="row">
                            <div className="col-md-9 col-sm-8 col-xs-12 pull-right">
                                <div className="content-grid-boxed">
                                    <FilterBar onSort={this.handleSort.bind(this)} />
                                    <ListProduct
                                        products={products}
                                        totalRecord={totalRows}
                                        limit={productPerPage}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={link.SEARCH}
                                        queryParams={params}
                                        onPageChange={this.handlePageChange.bind(this)}
                                        onAddToCart={this.handleAddToCart.bind(this)} />
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-4 col-xs-12">
                                <div className="sidebar sidebar-left">
                                    <ProductBestSelling bestSelling={hotProducts} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

SearchProduct.propTypes = {
    products: PropTypes.array.isRequired,
    hotProducts: PropTypes.array.isRequired,
    totalRows: PropTypes.number.isRequired,
    fetchSearchProductsRequest: PropTypes.func.isRequired,
    fetchHotProductsRequest: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    products: state.product.list,
    totalRows: state.product.totalRows,
    hotProducts: state.product.hotProducts,
});

const mapDispatchToProps = dispatch => ({
    fetchSearchProductsRequest: (slug, offset, limit, params) => dispatch(fetchSearchProductsRequest(slug, offset, limit, params)),
    fetchHotProductsRequest: limit => dispatch(fetchHotProductsRequest(limit)),
    addToCart: product => dispatch(addToCart(product)),
})

export default connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(SearchProduct)
);
