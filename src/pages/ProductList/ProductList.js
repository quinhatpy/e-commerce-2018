import PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { addToCart, fetchBestSellingByCategoryRequest, fetchBreadcrumbProductListRequest, fetchCategoryBannerInProductListRequest, fetchProductsRequest } from '../../actions';
import BannerSlider from '../../components/BannerSlider';
import Breadcrumb from '../../components/Breadcrumb';
import FilterBar from '../../components/FilterBar';
import GridProduct from '../../components/GridProduct';
import MetaTag from '../../components/MetaTag';
import ProductBestSelling from '../../components/ProductSelling';
import * as link from '../../constants/link';
import { getPageMeta } from '../../utils/helper';
import scrollTop from '../../utils/scrollTop';

const productPerPage = 16;

const frontLoad = async props => {
    await props.fetchBreadcrumbProductListRequest(props.match.params.slug);
    await props.fetchBestSellingByCategoryRequest(props.match.params.slug, 10);
    await props.fetchCategoryBannerInProductListRequest(props.match.params.slug, 5);
    const queryParams = queryString.parse(props.location.search);

    let offset = 0;
    if (props.match.params.page) {
        offset = (parseInt(props.match.params.page) - 1) * productPerPage;
    }
    await props.fetchProductsRequest(props.match.params.slug, offset, productPerPage, queryParams);
}

class ProductList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            currentPage: 1,
            offset: 0,
            params: {},
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.setState({ currentPage: parseInt(this.props.match.params.page) || 1 })
    }

    componentWillReceiveProps(nextProps) {
        const {
            match,
            fetchBreadcrumbProductListRequest
        } = this.props;

        if (nextProps.match.params.slug !== match.params.slug)
            fetchBreadcrumbProductListRequest(nextProps.match.params.slug)
    }

    handlePageChange(pageNumber, queryParams = '') {
        const {
            match,
            fetchProductsRequest
        } = this.props;
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * productPerPage;
        stateClone.offset = offset;
        this.setState(stateClone);

        fetchProductsRequest(match.params.slug, offset, productPerPage, stateClone.params);
        scrollTop();
    }

    handleSort(event) {
        const {
            history,
            match
        } = this.props;
        const sortBy = event.target.value;

        let stateClone = { ...this.state };
        stateClone.params.sort_by = sortBy;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        history.push({
            pathname: link.PRODUCT_LIST + match.params.slug,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleAddToCart(cartItem) {
        const {
            history,
            addToCart
        } = this.props;

        addToCart(cartItem);
        history.push({
            pathname: link.CART,
        });
    }

    render() {
        const {
            params,
            currentPage,
            offset
        } = this.state;
        const {
            breadcrumb,
            bestSelling,
            banners,
            products,
            totalRows,
            match,
        } = this.props;
        const pageMeta = getPageMeta(breadcrumb)

        return (
            <MetaTag {...pageMeta}>
                <div className="content-page">
                    <div className="container">
                        <Breadcrumb breadcrumb={breadcrumb} />

                        <div className="row">
                            <div className="col-md-9 col-sm-8 col-xs-12 pull-right">
                                <BannerSlider banners={banners} />
                                <div className="content-grid-boxed">
                                    <FilterBar onSort={this.handleSort.bind(this)} />
                                    <GridProduct
                                        products={products}
                                        totalRecord={totalRows}
                                        limit={productPerPage}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={link.PRODUCT_LIST + match.params.slug}
                                        queryParams={params}
                                        onPageChange={this.handlePageChange.bind(this)}
                                        onAddToCart={this.handleAddToCart.bind(this)}
                                    />
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-4 col-xs-12">
                                <div className="sidebar sidebar-left">
                                    <ProductBestSelling bestSelling={bestSelling} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

ProductList.propTypes = {
    breadcrumb: PropTypes.array.isRequired,
    banners: PropTypes.array.isRequired,
    bestSelling: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired,
    totalRows: PropTypes.number.isRequired,
    fetchCategoryBannerInProductListRequest: PropTypes.func.isRequired,
    fetchBreadcrumbProductListRequest: PropTypes.func.isRequired,
    fetchBestSellingByCategoryRequest: PropTypes.func.isRequired,
    fetchProductsRequest: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    banners: state.categoryBanner.banners,
    breadcrumb: state.breadcrumb,
    bestSelling: state.product.bestSelling,
    products: state.product.list,
    totalRows: state.product.totalRows
});

const mapDispatchToProps = dispatch => ({
    fetchCategoryBannerInProductListRequest: (slug, limit) => dispatch(fetchCategoryBannerInProductListRequest(slug, limit)),
    fetchBreadcrumbProductListRequest: slug => dispatch(fetchBreadcrumbProductListRequest(slug)),
    fetchBestSellingByCategoryRequest: (slug, limit) => dispatch(fetchBestSellingByCategoryRequest(slug, limit)),
    fetchProductsRequest: (slug, offset, limit, params) => dispatch(fetchProductsRequest(slug, offset, limit, params)),
    addToCart: product => dispatch(addToCart(product)),
})

export default connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(ProductList)
);
