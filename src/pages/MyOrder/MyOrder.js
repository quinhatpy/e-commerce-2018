import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { cancelOrderRequest, fetchMyOrderRequest } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import MetaTag from '../../components/MetaTag';
import * as link from '../../constants/link';
import { formatMoney } from '../../utils/helper';

class MyOrder extends Component {
    componentDidMount() {
        const {
            isAuthenticated,
            history,
            fetchMyOrderRequest
        } = this.props;

        window.scrollTo(0, 0);
        if (!isAuthenticated) {
            history.push({
                pathname: link.LOGIN,
                search: '?redirect=' + link.MY_ORDER
            })
        }

        fetchMyOrderRequest();
    }

    handleCancelOrder(code) {
        if (window.confirm('Bạn có chắc chắn muốn hủy đơn hàng này')) {
            this.props.cancelOrderRequest(code)
        }
    }

    renderItem(orders) {
        const orderStatus = ['Đơn hàng đã hủy', 'Đơn hàng mới', 'Đã xác nhận', 'Đang vận chuyển', 'Đã giao hàng'];

        if (orders.length === 0)
            return (
                <tr className="cart_item">
                    <td className="text-center" colSpan={7}>
                        Bạn chưa có đơn hàng nào
                    </td>
                </tr>
            )

        return orders.map((item, index) => (
            <tr className="cart_item" key={index}>
                <th className="product-remove">{index + 1}</th>
                <th className="product-thumbnail">{item.code}</th>
                <th className="text-center">{item.quantity}</th>
                <th className="text-right">{formatMoney(item.total)}<sup>đ</sup></th>
                <th className="product-quantity">{item.created_at}</th>
                <th className="product-subtotal">{orderStatus[item.status]}</th>
                <th className="product-subtotal">
                    <Link to={link.MY_ORDER_DETAIL + item.code} className="btn btn-info">Xem</Link> &nbsp;
                    {
                        item.status === 1 ? <button type="button" className="btn btn-danger" onClick={this.handleCancelOrder.bind(this, item.code)}>Hủy</button> : null
                    }
                </th>
            </tr>
        ))
    }

    render() {
        const breadcrumb = [{ name: 'Đơn hàng của tôi' }];
        const { orders } = this.props;

        return (
            <MetaTag title="Đơn hàng của tôi">
                <div className="content-page woocommerce">
                    <div className="container register-content-box">
                        <Breadcrumb breadcrumb={breadcrumb} />
                        <h2 className="title-shop-page">Các đơn hàng của bạn</h2>
                        <div className="cart-content-page">
                            <div className="table-responsive">
                                <table cellSpacing={0} className="shop_table cart table">
                                    <thead>
                                        <tr>
                                            <th className="product-remove">#</th>
                                            <th className="product-thumbnail">Mã đơn hàng</th>
                                            <th className="product-quantity">Số lượng sản phẩm</th>
                                            <th className="product-price">Tổng tiền</th>
                                            <th className="product-name">Ngày đặt</th>
                                            <th className="product-quantity">Tình trạng</th>
                                            <th className="product-remove"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderItem(orders)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag >
        )
    }
}

MyOrder.propTypes = {
    orders: PropTypes.array.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    fetchMyOrderRequest: PropTypes.func.isRequired,
    cancelOrderRequest: PropTypes.func.isRequired,
}


const mapStateToProps = state => ({
    orders: state.order.list,
    isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
    fetchMyOrderRequest: () => dispatch(fetchMyOrderRequest()),
    cancelOrderRequest: code => dispatch(cancelOrderRequest(code))
})

export default connect(mapStateToProps, mapDispatchToProps)(MyOrder);
