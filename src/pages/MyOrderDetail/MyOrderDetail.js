import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchMyOrderDetailRequest } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import MetaTag from '../../components/MetaTag';
import * as link from '../../constants/link';
import { formatMoney } from '../../utils/helper';

class MyOrderDetail extends Component {
    componentDidMount() {
        const {
            isAuthenticated,
            history,
            match,
            fetchMyOrderDetailRequest
        } = this.props;

        window.scrollTo(0, 0);
        if (!isAuthenticated) {
            history.push({
                pathname: link.LOGIN,
            })
        }

        fetchMyOrderDetailRequest(match.params.code)
    }

    renderCartItem(orderDetail) {
        return orderDetail.map((item, index) => (
            <tr className="cart_item" key={index}>
                <td className="product-remove">
                    {index + 1}
                </td>
                <td className="product-thumbnail">
                    <Link to={link.PRODUCT_DETAIL + item.product.slug}><img src={item.product.thumbnail} alt={item.product.name} /></Link>
                </td>
                <td className="product-name">
                    <Link to={link.PRODUCT_DETAIL + item.product.slug}>{item.product.name}</Link>
                </td>
                <td className="product-price">
                    <span className="amount">{formatMoney(item.price)}<sup>đ</sup></span>
                </td>
                <td className="product-quantity">
                    <span className="amount">{item.quantity}</span>
                </td>
                <td className="product-subtotal">
                    <span className="amount">{formatMoney(item.price * item.quantity)}<sup>đ</sup></span>
                </td>
            </tr>
        ))
    }

    countTotal(cart) {
        return cart.reduce((total, item) => total += item.price * item.quantity, 0)
    }

    render() {
        const breadcrumb = [{ name: 'Thông tin đơn hàng' }];
        const { order } = this.props;

        return (
            <MetaTag title="Thông tin đơn hàng">
                <div className="content-page woocommerce">
                    <div className="container register-content-box">
                        <Breadcrumb breadcrumb={breadcrumb} />
                        <h2 className="title-shop-page">Thông tin đơn hàng</h2>
                        <div className="form-my-account col-md-4">
                            <form method="post" className="block-login">
                                <h2 className="title18 text-center">Thông tin đặt hàng</h2>
                                <p>
                                    <label htmlFor="receiver_name">Tên người nhận <span className="required">*</span></label>
                                    <input type="text" name="receiver_name" defaultValue={order.receiver_name} readOnly />
                                </p>
                                <p>
                                    <label htmlFor="receiver_phone">Số điện thoại người nhận <span className="required">*</span></label>
                                    <input type="text" name="receiver_phone" defaultValue={order.receiver_phone} readOnly />
                                </p>
                                <p>
                                    <label htmlFor="receiver_address">Địa chỉ người nhận <span className="required">*</span></label>
                                    <textarea name="receiver_address" value={order.receiver_address} readOnly></textarea>
                                </p>
                                <p>
                                    <label htmlFor="note">Ghi chú<span className="required">*</span></label>
                                    <textarea name="note" value={order.note} readOnly></textarea>
                                </p>
                            </form>
                        </div>
                        <div className="cart-content-page col-md-7">
                            <h2 className="title18 text-center">Các sản phẩm đã chọn</h2>
                            <br />
                            <div className="table-responsive">
                                <table cellSpacing={0} className="shop_table cart table">
                                    <thead>
                                        <tr>
                                            <th className="product-remove">#</th>
                                            <th className="product-thumbnail">Hình</th>
                                            <th className="product-name">Tên sản phẩm</th>
                                            <th className="product-price">Giá</th>
                                            <th className="product-quantity">Số lượng</th>
                                            <th className="product-subtotal">Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderCartItem(order.order_details || [])}
                                        {
                                            order.order_details && order.order_details.length > 0 ?
                                                <tr>
                                                    <td className="actions" colSpan={7}>
                                                        <label htmlFor="coupon_code">Tổng cộng: </label>
                                                        <span>{formatMoney(this.countTotal(order.order_details))}<sup>đ</sup></span>
                                                    </td>
                                                </tr>
                                                : null
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag >
        )
    }
}

MyOrderDetail.propTypes = {
    order: PropTypes.shape({
        id: PropTypes.number,
        code: PropTypes.string,
        receiver_name: PropTypes.string,
        receiver_phone: PropTypes.string,
        receiver_address: PropTypes.string,
        note: PropTypes.string,
        status: PropTypes.string,
        is_paid: PropTypes.string,
        created_at: PropTypes.string,
        order_details: PropTypes.array
    }).isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    fetchMyOrderDetailRequest: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    order: state.order.item,
    isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
    fetchMyOrderDetailRequest: code => dispatch(fetchMyOrderDetailRequest(code))
})

export default connect(mapStateToProps, mapDispatchToProps)(MyOrderDetail);
