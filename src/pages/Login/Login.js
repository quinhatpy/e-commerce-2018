import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { loginRequest } from '../../actions';
import Alert from '../../components/Alert';
import MetaTag from '../../components/MetaTag';
import * as link from '../../constants/link';

class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: ''
        }

        this.emailRef = React.createRef();
        this.passwordRef = React.createRef();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    handleSubmit(event) {
        event.preventDefault();
        if (window.navigator.onLine) {
            let isValid = true;
            const {
                email,
                password
            } = this.state;

            isValid = this.validateInputData('email', email) && isValid;
            isValid = this.validateInputData('password', password) && isValid;
            if (isValid) {
                let formData = new FormData(event.target);
                this.props.loginRequest(formData);
            }
        }
        else {
            alert('Vui lòng kết nối internet!');
        }
    }

    validateInputData(input, data) {
        let valid = true;

        if (input === 'email') {
            this.emailRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.emailRef.current.innerText = 'Email không được để trống.';
            }
        }

        if (input === 'password') {
            this.passwordRef.current.innerText = '';
            if (data.length === 0) {
                valid = false;
                this.passwordRef.current.innerText = 'Mật khẩu không được để trống.';
            }
        }
        return valid;
    }

    handleInputChange(event) {
        const target = event.target;
        this.validateInputData(target.name, target.value);

        this.setState({
            [target.name]: target.value
        });
    }

    render() {
        const { alert } = this.props;

        return (
            <MetaTag title="Đăng nhập">
                <div className="content-page woocommerce">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="register-content-box">
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-ms-12 col-sm-offset-3">
                                            <div className="check-billing">
                                                <div className="form-my-account">
                                                    <form className="block-login" onSubmit={this.handleSubmit.bind(this)}>
                                                        <h2 className="title24 title-form-account text-center">Đăng nhập</h2>
                                                        <Alert type={alert.type} title={alert.title} content={alert.content} />
                                                        <p>
                                                            <label htmlFor="username">Email <span className="required">*</span></label>
                                                            <input type="email" name="email" onChange={this.handleInputChange.bind(this)} />
                                                            <span className="text-danger" ref={this.emailRef}></span>
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password">Mật khẩu <span className="required">*</span></label>
                                                            <input type="password" name="password" onChange={this.handleInputChange.bind(this)} />
                                                            <span className="text-danger" ref={this.passwordRef}></span>
                                                        </p>
                                                        <p>
                                                            <input type="submit" className="register-button" name="login" value="Đăng nhập" />
                                                        </p>
                                                        <div className="table create-account">
                                                            <div className="text-right">
                                                                <Link to={link.RESET_PASSWORD_REQUEST} className="color">Bạn quên mật khẩu?</Link>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

Login.propTypes = {
    alert: PropTypes.object,
    loginRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    alert: state.alert
})

const mapDispatchToProps = dispatch => ({
    loginRequest: (data) => dispatch(loginRequest(data))
})


export default connect(mapStateToProps, mapDispatchToProps)(Login);
