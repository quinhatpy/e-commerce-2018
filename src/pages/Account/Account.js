import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateCustomerRequest } from '../../actions';
import Alert from '../../components/Alert';
import Breadcrumb from '../../components/Breadcrumb';
import MetaTag from '../../components/MetaTag';

class Account extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
        if (!this.props.isAuthenticated) {
            this.props.history.push({
                pathname: '/dang-nhap',
                search: '?redirect=tai-khoan'
            })
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('_method', 'PUT');

        this.props.updateCustomerRequest(formData);
    }

    render() {
        const {
            alert,
            user
        } = this.props;

        return (
            <MetaTag title="Thông tin tài khoản">
                <div className="content-page woocommerce">
                    <div className="container">
                        <Breadcrumb breadcrumb={[{ name: 'Thông tin tài khoản' }]} />
                        <div className="row">
                            <div className="col-md-12">
                                <div className="register-content-box">
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-ms-12 col-sm-offset-3">
                                            <div className="check-billing">
                                                <div className="form-my-account">
                                                    <form className="block-login" onSubmit={this.handleSubmit.bind(this)}>
                                                        <h2 className="title24 title-form-account text-center">Thông tin tài khoản</h2>
                                                        <Alert type={alert.type} title={alert.title} content={alert.content} />
                                                        <p>
                                                            <label htmlFor="username">Email</label>
                                                            <input type="email" disabled defaultValue={user.email} />
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password">Họ và tên <span className="required">*</span></label>
                                                            <input type="text" name="name" defaultValue={user.name} />
                                                        </p>
                                                        <p>
                                                            <label htmlFor="address">Địa chỉ</label>
                                                            <textarea name="address" defaultValue={user.address}></textarea>
                                                        </p>
                                                        <p>
                                                            <label htmlFor="phone">Số điện thoại</label>
                                                            <input type="text" name="phone" defaultValue={user.phone} />
                                                        </p>
                                                        <h3 className="title18 title-change-pasword">Thay đổi mật khẩu</h3>
                                                        <p>
                                                            <label htmlFor="password">Mật khẩu mới (để trống nếu không cập nhật)</label>
                                                            <input type="password" name="password" />
                                                        </p>
                                                        <p>
                                                            <label htmlFor="password">Nhập lại mật khẩu mới </label>
                                                            <input type="password" name="password_confirmation" />
                                                        </p>
                                                        <p>
                                                            <input type="submit" className="register-button" name="login" value="Cập nhật" />
                                                        </p>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

Account.propTypes = {
    alert: PropTypes.object,
    user: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired
    }).isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    updateCustomerRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    alert: state.alert,
    user: state.auth.user,
    isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = dispatch => ({
    updateCustomerRequest: (data) => dispatch(updateCustomerRequest(data)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Account);
