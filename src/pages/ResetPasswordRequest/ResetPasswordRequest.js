import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createResetPasswordRequest } from '../../actions';
import Alert from '../../components/Alert';
import MetaTag from '../../components/MetaTag';

class ResetPasswordRequest extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
        }

        this.emailRef = React.createRef();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    handleSubmit(event) {
        event.preventDefault();
        if (window.navigator.onLine) {
            const {
                history,
                createResetPasswordRequest
            } = this.props;

            if (this.validateInputData(this.state.email)) {
                let formData = new FormData(event.target);
                createResetPasswordRequest(formData, history);
            }

        } else alert('Vui lòng kết nối internet!');
    }

    handleInputChange(event) {
        this.validateInputData(event.target.value);

        this.setState({
            email: event.target.value
        });
    }

    validateInputData(data) {
        this.emailRef.current.innerText = '';
        if (data.length === 0) {
            this.emailRef.current.innerText = 'Email không được để trống!';
            return false
        }

        return true;
    }

    render() {
        const { alert } = this.props;

        return (
            <MetaTag title="Khôi phục mật khẩu">
                <div className="content-page woocommerce">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="register-content-box">
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-ms-12 col-sm-offset-3">
                                            <div className="check-billing">
                                                <div className="form-my-account">
                                                    <form className="block-login" onSubmit={this.handleSubmit.bind(this)}>
                                                        <h2 className="title24 title-form-account text-center">Khôi phục mật khẩu</h2>
                                                        <Alert type={alert.type} title={alert.title} content={alert.content} />
                                                        <p>
                                                            <label htmlFor="username">Email <span className="required">*</span></label>
                                                            <input type="email" name="email" placeholder="Nhập email để khôi phục mật khẩu" 
                                                                onChange={this.handleInputChange.bind(this)}/>
                                                            <span className="text-danger" ref={this.emailRef}></span>
                                                        </p>
                                                        <p>
                                                            <input type="submit" className="register-button" name="login" value="Lấy lại mật khẩu" />
                                                        </p>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

ResetPasswordRequest.propTypes = {
    alert: PropTypes.object,
    createResetPasswordRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    alert: state.alert
})

const mapDispatchToProps = dispatch => ({
    createResetPasswordRequest: (data, history) => dispatch(createResetPasswordRequest(data, history))
})


export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordRequest);
