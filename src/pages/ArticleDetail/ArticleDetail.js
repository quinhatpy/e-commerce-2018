import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchArticleRequest, fetchSuperDealsRequest } from '../../actions';
import Breadcrumb from '../../components/Breadcrumb';
import MetaTag from '../../components/MetaTag';
import SuperDeal from '../../components/SuperDeal';
import * as link from '../../constants/link';

const frontLoad = async props => {
    await props.fetchSuperDealsRequest(10);
    await props.fetchArticleRequest(props.match.params.slug);
}

class ArticleDetail extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        const {
            superDeals,
            article
        } = this.props;

        return (
            <MetaTag title={article.seo_title || article.name} description={article.seo_description}>
                <div className="content-page">
                    <div className="container">
                        <Breadcrumb breadcrumb={[{ name: article.name }]} />
                        <div className="row">
                            <div className="col-md-9 col-sm-8 col-xs-12 pull-right">
                                <div className="content-single single-with-sidebar">
                                    <div className="main-single">
                                        <div className="post-format-date">
                                            <i aria-hidden="true" className="fa fa-calendar-o" />
                                            <span>{article.created_at}</span>
                                        </div>
                                        <h2 className="title-single">{article.name}</h2>
                                        <p className="desc post-intro">{article.short_description}</p>
                                        <ul className="post-date-comment">
                                            <li><i aria-hidden="true" className="fa fa-user" /><label>Đăng bởi:</label>{article.user ? article.user.name : null}</li>
                                        </ul>
                                    </div>
                                    <div className="post-thumb">
                                        <div className="wrap-item1">
                                            <Link to={link.ARTICLE_DETAIL + article.slug} className="post-thumb-link"><img src={article.thumbnail} alt={article.name} /></Link>
                                        </div>
                                    </div>
                                    <div className="desc" dangerouslySetInnerHTML={{ __html: article.content }}></div>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-4 col-xs-12">
                                <div className="sidebar sidebar-left">
                                    <SuperDeal superDeals={superDeals} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MetaTag>
        )
    }
}

ArticleDetail.propTypes = {
    superDeals: PropTypes.array.isRequired,
    article: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        short_description: PropTypes.string,
        slug: PropTypes.string,
        thumbnail: PropTypes.string,
        created_at: PropTypes.string,
        seo_description: PropTypes.string,
        seo_title: PropTypes.string,
        content: PropTypes.string,
    }).isRequired,
    fetchSuperDealsRequest: PropTypes.func.isRequired,
    fetchArticleRequest: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    superDeals: state.product.superDeals,
    article: state.article.item
});

const mapDispatchToProps = dispatch => ({
    fetchSuperDealsRequest: limit => dispatch(fetchSuperDealsRequest(limit)),
    fetchArticleRequest: slug => dispatch(fetchArticleRequest(slug)),
})

export default connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(ArticleDetail)
);
