import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { addToCart, fetchBannersRequest, fetchBrandsInHomeRequest, fetchCategoriesRequest, fetchCategoryBannerInHomeRequest, fetchHotProductsRequest, fetchProductBoxesRequest, fetchSuperDealsRequest } from '../../actions';
import HotProduct from '../../components/HotProduct';
import MetaTag from '../../components/MetaTag';
import ProductBox from '../../components/ProductBox';
import * as link from '../../constants/link';
import Brand from '../../containers/Brand';
import ContentTop from '../../containers/ContentTop';

const categoryIdArray = [9, 2, 5, 7];

const frontLoad = async props => {
    if (isEmpty(props.categories))
        await props.fetchCategoriesRequest();
    if (isEmpty(props.homeBanners))
        await props.fetchBannersRequest('home', 5);
    if (isEmpty(props.superDeals))
        await props.fetchSuperDealsRequest(4);
    if (isEmpty(props.hotProducts))
        await props.fetchHotProductsRequest(10);
    if (isEmpty(props.productBoxes))
        await props.fetchProductBoxesRequest(8, categoryIdArray);
    if (isEmpty(props.categoryBanners))
        await props.fetchCategoryBannerInHomeRequest(categoryIdArray);
    if (isEmpty(props.brands))
        await props.fetchBrandsInHomeRequest(20);
}

class HomePage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            productBoxColor: ['', 'block-green', 'block-yellow', 'block-purple']
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);

    }

    renderProductBox(categories, productBoxes, categoryBanners) {
        const { productBoxColor } = this.state;

        const categoryParents = categories.filter(category => categoryIdArray.includes(category.id));

        return categoryParents.map((category, index) => {

            const categoryChildren = categories.filter(item => parseInt(item.parent_id) === category.id);

            let productData = productBoxes.data ? productBoxes.data[category.id] : {};
            let categoryBanner = categoryBanners.data ? categoryBanners.data[category.id] : {};
            return <ProductBox
                key={category.id}
                productBoxColor={productBoxColor[index]}
                categoryName={category.name}
                categoryChildren={categoryChildren}
                productBoxes={productData}
                categoryBanner={categoryBanner}
                onAddToCart={this.handleAddToCart.bind(this)} />
        });
    }

    handleAddToCart(cartItem) {
        this.props.addToCart(cartItem);
        this.props.history.push({
            pathname: link.CART,
        });
    }

    render() {
        const {
            categories,
            homeBanners,
            superDeals,
            hotProducts,
            productBoxes,
            categoryBanners,
            brands,
        } = this.props;

        const superDealList = superDeals.slice(0, 4);
        return (
            <MetaTag>
                <div className="container">
                    <ContentTop
                        categories={categories}
                        homeBanners={homeBanners}
                        superDeals={superDealList} />
                    <HotProduct hotProducts={hotProducts} onAddToCart={this.handleAddToCart.bind(this)} />
                </div>
                {this.renderProductBox(categories, productBoxes, categoryBanners)}
                <Brand brands={brands} />
            </MetaTag>
        )
    }
}

HomePage.propTypes = {
    categories: PropTypes.array.isRequired,
    homeBanners: PropTypes.array.isRequired,
    superDeals: PropTypes.array.isRequired,
    hotProducts: PropTypes.array.isRequired,
    productBoxes: PropTypes.object.isRequired,
    categoryBanners: PropTypes.object.isRequired,
    brands: PropTypes.array.isRequired,
    fetchCategoriesRequest: PropTypes.func.isRequired,
    fetchBannersRequest: PropTypes.func.isRequired,
    fetchSuperDealsRequest: PropTypes.func.isRequired,
    fetchHotProductsRequest: PropTypes.func.isRequired,
    fetchProductBoxesRequest: PropTypes.func.isRequired,
    fetchCategoryBannerInHomeRequest: PropTypes.func.isRequired,
    fetchBrandsInHomeRequest: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    categories: state.category.list,
    homeBanners: state.banner.home,
    superDeals: state.product.superDeals,
    hotProducts: state.product.hotProducts,
    productBoxes: state.product.productBoxes,
    categoryBanners: state.categoryBanner.bannerInHome,
    brands: state.brand.brandInHome
});

const mapDispatchToProps = dispatch => ({
    fetchCategoriesRequest: () => dispatch(fetchCategoriesRequest()),
    fetchBannersRequest: (position, limit) => dispatch(fetchBannersRequest(position, limit)),
    fetchSuperDealsRequest: limit => dispatch(fetchSuperDealsRequest(limit)),
    fetchHotProductsRequest: limit => dispatch(fetchHotProductsRequest(limit)),
    fetchProductBoxesRequest: (limit, categoryIdArray) => dispatch(fetchProductBoxesRequest(limit, categoryIdArray)),
    fetchCategoryBannerInHomeRequest: categoryIdArray => dispatch(fetchCategoryBannerInHomeRequest(categoryIdArray)),
    fetchBrandsInHomeRequest: limit => dispatch(fetchBrandsInHomeRequest(limit)),
    addToCart: product => dispatch(addToCart(product)),
})

export default connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(HomePage)
);
