import { ConnectedRouter } from 'connected-react-router';
import Cookies from 'js-cookie';
import React from 'react';
import ReactDOM from 'react-dom';
import { Frontload } from 'react-frontload';
import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import App from './App';
import './index.css';
import * as serviceWorker from './serviceWorker';
import createStore from './store';
import api from './utils/axios';

// Create a store and get back itself and its history object
const { store, history } = createStore();


// Running locally, we should run on a <ConnectedRouter /> rather than on a <StaticRouter /> like on the server
// Let's also let React Frontload explicitly know we're not rendering on the server here
const Application = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Frontload noServerRender={true}>
                <App />
            </Frontload>
        </ConnectedRouter>
    </Provider>
);

const root = document.querySelector('#root');

const token = Cookies.get('accessToken');
if (token)
    api.defaults.headers.common['Authorization'] = `Bearer ${token}`;

if (root.hasChildNodes() === true) {
    // If it's an SSR, we use hydrate to get fast page loads by just
    // attaching event listeners after the initial render
    Loadable.preloadReady().then(() => {
        ReactDOM.hydrate(Application, root);
    });
} else {
    // If we're not running on the server, just render like normal
    ReactDOM.render(Application, root);
}


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
