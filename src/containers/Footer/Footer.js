import React, { Component } from 'react';
import { urlBase64ToUint8Array } from '../../utils/helper';
import { subscriptionRequest } from '../../actions';
import { connect } from 'react-redux';

class Footer extends Component {

    handleSubscription() {
        const { subscriptionRequest } = this.props;

        Notification.requestPermission(function (result) {
            console.log('User Choice', result);
            if (result !== 'granted') {
                console.log('No notification permission granted!');
            } else {
                console.log('ACCEPT Notification');
                if (!('serviceWorker' in navigator)) {
                    return;
                }

                var reg;
                navigator.serviceWorker.ready
                    .then(function (swreg) {
                        console.log('SERVICE WORKER READY', swreg);
                        reg = swreg;
                        return swreg.pushManager.getSubscription();
                    })
                    .then(function (sub) {
                        console.log('REGISTER SUBSCRIPTION', sub);
                        if (sub === null) {
                            // Create a new subscription
                            var vapidPublicKey = 'BKhudoZnOOV4yQs4HUR78fuf5BFToq6zfbfKdaoF7JlUNREOl6VAkr4YEf3e7Zc30L5kAO4o+fE2AzQbiXN9CWU=';
                            var convertedVapidPublicKey = urlBase64ToUint8Array(vapidPublicKey);
                            return reg.pushManager.subscribe({
                                userVisibleOnly: true,
                                applicationServerKey: convertedVapidPublicKey
                            });
                        } else {
                            alert('Bạn đã đăng ký nhận thông báo rồi!')
                        }
                    })
                    .then(function (newSub) {
                        console.log(newSub);
                        const key = newSub.getKey('p256dh')
                        const token = newSub.getKey('auth')

                        const data = {
                            endpoint: newSub.endpoint,
                            key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
                            token: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null
                        }
                        console.log('DATA', data);
                        subscriptionRequest(data);
                    })
            }
        });
    }

    render() {
        const { isAuthenticated } = this.props;

        return (
            <div id="footer">
                <div className="footer footer1">
                    <div className="container">
                        <div className="service-footer">
                            <div className="row">
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                    <div className="item-service-footer">
                                        <div className="service-icon">
                                            <a href="/" className="wobble-horizontal"><img className="img-circle" src="/images/piggy-bank.png" alt="" /></a>
                                        </div>
                                        <div className="service-info">
                                            <h2 className="title14">Business Identity</h2>
                                            <p>Get Verified, Build Trust</p>
                                            <ul className="list-unstyled">
                                                <li><a href="/">Display your verification status</a></li>
                                                <li><a href="/">Faster responses from suppliers when verified</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                    <div className="item-service-footer">
                                        <div className="service-icon">
                                            <a href="/" className="wobble-horizontal"><img className="img-circle" src="/images/investment.png" alt="" /></a>
                                        </div>
                                        <div className="service-info">
                                            <h2 className="title14">e - Credit Line</h2>
                                            <p>Buy Now, Pay Later</p>
                                            <ul className="list-unstyled">
                                                <li><a href="/">Trusted local lenders</a></li>
                                                <li><a href="/">Trade financing in minutes</a></li>
                                                <li><a href="/">Competitive rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                    <div className="item-service-footer">
                                        <div className="service-icon">
                                            <a href="/" className="wobble-horizontal"><img className="img-circle" src="/images/telemarketer.png" alt="" /></a>
                                        </div>
                                        <div className="service-info">
                                            <h2 className="title14">Inspection Service</h2>
                                            <p>Have Your Order Inspected</p>
                                            <ul className="list-unstyled">
                                                <li><a href="/">Independent inspectors</a></li>
                                                <li><a href="/">Proven cost efficiency</a></li>
                                                <li><a href="/">Competitive rates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* End Service Footer */}
                        <div className="footer-list-box">
                            <div className="row">
                                <div className="col-md-5 col-sm-12 col-xs-12">
                                    <div className="newsletter-form footer-box">
                                        <h2 className="title14">Subscription</h2>
                                        {
                                            isAuthenticated ?
                                                <input type="button" value="Đăng ký nhận thông báo" onClick={this.handleSubscription.bind(this)} />
                                                : <p>Vui lòng đăng nhập để đăng ký nhận thông báo</p>
                                        }

                                    </div>
                                    <div className="social-footer footer-box">
                                        <h2 className="title14">Stay Connected</h2>
                                        <div className="list-social">
                                            <a href="/"><i className="fa fa-facebook" aria-hidden="true" /> </a>
                                            <a href="/"><i className="fa fa-twitter" aria-hidden="true" /> </a>
                                            <a href="/"><i className="fa fa-linkedin" aria-hidden="true" /> </a>
                                            <a href="/"><i className="fa fa-pinterest" aria-hidden="true" /> </a>
                                            <a href="/"><i className="fa fa-google-plus" aria-hidden="true" /> </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 col-sm-3 col-xs-6">
                                    <div className="menu-footer-box footer-box">
                                        <h2 className="title14">How to Buy</h2>
                                        <ul className="list-unstyled">
                                            <li><a href="/">Create an Account</a></li>
                                            <li><a href="/">Making Payments</a></li>
                                            <li><a href="/">Delivery Options</a></li>
                                            <li><a href="/">Buyer Protection</a></li>
                                            <li><a href="/">New User Guide</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-2 col-sm-3 col-xs-6">
                                    <div className="menu-footer-box footer-box">
                                        <h2 className="title14">Customer Service</h2>
                                        <ul className="list-unstyled">
                                            <li><a href="/">Customer Support</a></li>
                                            <li><a href="/">Affiliate Program</a></li>
                                            <li><a href="/">Blog</a></li>
                                            <li><a href="/">About us</a></li>
                                            <li><a href="/">Contact us</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-3 col-sm-6 col-xs-12">
                                    <div className="contact-footer-box footer-box">
                                        <h2 className="title14">contact us</h2>
                                        <p className="formule-address">8901 Marmora Road, Glasgow, D04  89GR.</p>
                                        <p className="formule-phone">+1 800 559 6580<br />+1 504 889 9898</p>
                                        <p className="formule-email"><a href="mailto:email@demolink.org">email@demolink.org</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* End Footer List Box */}
                    </div>

                    <div className="footer-copyright">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 col-sm-8 col-xs-12">
                                    <p className="copyright">ECOMMERCE © 2018 NhatLe.Net. All rights reserved.</p>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                    <p className="designby">Design by: <a href="/">NhatLe.Net</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Footer Copyright */}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
    subscriptionRequest: data => dispatch(subscriptionRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);