import Cookies from 'js-cookie';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { frontloadConnect } from 'react-frontload';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { fetchCategoriesRequest, fetchMenusRequest, initCart, logoutRequest, removeCartItem, updateCartItem, fetchUserCurrentRequest } from '../../actions';
import CartBox from '../../components/CartBox';
import HeaderOnTop from '../../components/HeaderOnTop';
// import TopBanner from '../../components/TopBanner';
import Logo from '../../components/Logo';
import MainNavigation from '../../components/MainNavigation';
import SearchBox from '../../components/SearchBox';
import * as link from '../../constants/link';

const frontLoad = async props => {
    if (isEmpty(props.categories))
        await props.fetchCategoriesRequest();
    if (isEmpty(props.menus))
        await props.fetchMenusRequest();
    if (Cookies.get('accessToken'))
        await props.fetchUserCurrentRequest();
}

class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            categorySelected: 0,
            searchKeyword: ''
        }
    }

    componentDidMount() {
        const {
            location,
            initCart
        } = this.props;

        const queryParams = queryString.parse(location.search);
        this.setState({
            categorySelected: !isEmpty(queryParams.category_id) ? parseInt(queryParams.category_id) : 0,
            searchKeyword: queryParams.keyword
        })
        initCart();
    }

    handleChangeCategorySelect(categorySelected) {
        this.setState({ categorySelected });
    }

    handleChangeSearchKeyword(event) {
        this.setState({ searchKeyword: event.target.value });
    }

    handleOnSearch(event) {
        event.preventDefault();
        const { categorySelected, searchKeyword } = this.state;
        const queryParam = {
            category_id: categorySelected,
            keyword: searchKeyword
        };

        this.props.history.push({
            pathname: link.SEARCH,
            search: '?' + queryString.stringify(queryParam)
        })
    }

    handleLogout() {
        const {
            history,
            logoutRequest
        } = this.props;

        logoutRequest();
        history.push({
            pathname: '/',
        })
    }

    render() {
        const {
            categorySelected,
            searchKeyword
        } = this.state;
        const {
            menus,
            categories,
            cart,
            updateCartItem,
            removeCartItem,
            isAuthenticated,
            user
        } = this.props;

        return (
            <div id="header">
                <div className="header">
                    <div className="main-header2 main-header12">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-3 col-sm-3 col-xs-12">
                                    <Logo />
                                </div>
                                <div className="col-md-5 col-sm-6 col-xs-12">
                                    <div className="wrap-search1">
                                        <SearchBox
                                            categories={categories}
                                            categorySelected={categorySelected}
                                            searchKeyword={searchKeyword}
                                            changeCategorySelect={this.handleChangeCategorySelect.bind(this)}
                                            changeSearchKeyword={this.handleChangeSearchKeyword.bind(this)}
                                            onSearch={this.handleOnSearch.bind(this)}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-3 col-xs-9">
                                    <div className="cart-social clearfix">
                                        <CartBox cart={cart} onUpdateCartItem={updateCartItem} onRemoveCartItem={removeCartItem} />

                                        <div className="social-header style2">
                                            <div className="checkout-box">

                                                {isAuthenticated ?
                                                    (
                                                        <Fragment>
                                                            <span className="link">Xin chào <b>{user.name}</b> </span>
                                                            <ul className="list-checkout list-unstyled">
                                                                <li><Link to={link.ACCOUNT}><i className="fa fa-user" />Tài khoản</Link></li>
                                                                <li><Link to={link.MY_ORDER}><i className="fa fa-key" aria-hidden="true" />Đơn hàng</Link></li>
                                                                <li><span className="link"
                                                                    onClick={this.handleLogout.bind(this)}><i className="fa fa-sign-in" /> Đăng xuất</span></li>
                                                            </ul>
                                                        </Fragment>
                                                    ) :
                                                    (
                                                        <Fragment>
                                                            <span className="link">Tài khoản của bạn</span>
                                                            <ul className="list-checkout list-unstyled">
                                                                <li><Link to={link.LOGIN}><i className="fa fa-user" /> Đăng nhập</Link></li>
                                                                <li><Link to={link.REGISTER}><i className="fa fa-key" aria-hidden="true" />Đăng ký</Link></li>
                                                            </ul>
                                                        </Fragment>
                                                    )}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="header-nav9 header-nav12">
                        <div className="container">
                            <MainNavigation menus={menus} />
                        </div>
                    </div>
                </div>
                <HeaderOnTop
                    menus={menus}
                    cart={cart}
                    onUpdateCartItem={updateCartItem}
                    onRemoveCartItem={removeCartItem}
                    searchKeyword={searchKeyword}
                    changeSearchKeyword={this.handleChangeSearchKeyword.bind(this)}
                    onSearch={this.handleOnSearch.bind(this)}
                    isAuthenticated={isAuthenticated}
                    handleLogout={this.handleLogout.bind(this)} />

            </div>
        )
    }
}

Header.propTypes = {
    menus: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
    menus: state.menu,
    categories: state.category.list,
    cart: state.cart,
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
    fetchMenusRequest: () => dispatch(fetchMenusRequest()),
    fetchCategoriesRequest: () => dispatch(fetchCategoriesRequest()),
    updateCartItem: (index, quantity) => dispatch(updateCartItem(index, quantity)),
    removeCartItem: index => dispatch(removeCartItem(index)),
    initCart: () => dispatch(initCart()),
    logoutRequest: () => dispatch(logoutRequest()),
    fetchUserCurrentRequest: () => dispatch(fetchUserCurrentRequest()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(
    frontloadConnect(frontLoad, {
        onMount: true,
        onUpdate: false
    })(Header))
)
