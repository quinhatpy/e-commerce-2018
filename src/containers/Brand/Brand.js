import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Slider from "react-slick";
import ItemBrand from '../../components/ItemBrand';

const settings = {
    speed: 500,
    slidesToShow: 8,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2
            }
        },
    ]
};

export default class Brand extends Component {
    renderItemBrand(brands) {
        return brands.map(brand => (
            <ItemBrand key={brand.id} brand={brand} />
        ));
    }
    render() {
        const { brands } = this.props;

        return (
            <div className="brand">
                <div className="container">
                    <div className="brand-slider10">
                        <div className="wrap-item1">
                            <div>
                                <Slider {...settings}>
                                    {this.renderItemBrand(brands)}
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Brand.propTypes = {
    brands: PropTypes.array.isRequired
}
