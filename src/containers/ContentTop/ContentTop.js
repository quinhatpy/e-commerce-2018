import PropTypes from 'prop-types';
import React, { Component } from 'react';
import BannerSlider from '../../components/BannerSlider';
import CategoryMenu from '../../components/CategoryMenu';
import SuperDeal from '../../components/SuperDeal';

export default class ContentTop extends Component {
    render() {
        const { 
            categories,
             homeBanners,
              superDeals 
            } = this.props;
        
        return (
            <div className="content-top12">
                <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <CategoryMenu categories={categories} />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <BannerSlider banners={homeBanners} />
                    </div>
                    <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <SuperDeal superDeals={superDeals} />
                    </div>
                </div>
            </div>
        )
    }
}

ContentTop.propTypes = {
    categories: PropTypes.array.isRequired,
    homeBanners: PropTypes.array.isRequired,
    superDeals: PropTypes.array.isRequired
}
