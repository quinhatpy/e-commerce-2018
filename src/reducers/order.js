import { FETCH_MY_ORDERS, FETCH_MY_ORDER_DETAIL } from '../constants/actionType';

const initialState = {
    list: [],
    item: {}
};

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_MY_ORDERS: {
            const cloneState = { ...state };
            cloneState.list = payload;
            return cloneState;
        }

        case FETCH_MY_ORDER_DETAIL: {
            const cloneState = { ...state };
            cloneState.item = payload;
            return cloneState;
        }

        default:
            return state
    }
}
