import { FETCH_CATEGORY_BANNERS_IN_HOME, FETCH_CATEGORY_BANNERS_IN_PRODUCT_LIST } from '../constants/actionType';

const initialState = {
    bannerInHome: {},
    banners: [],
};

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_CATEGORY_BANNERS_IN_HOME: {
            const cloneState = { ...state };
            cloneState.bannerInHome = payload;
            return cloneState;
        }

        case FETCH_CATEGORY_BANNERS_IN_PRODUCT_LIST: {
            const cloneState = { ...state };
            cloneState.banners = payload;
            return cloneState;
        }

        default:
            return state
    }
}
