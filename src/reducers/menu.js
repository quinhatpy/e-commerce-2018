import { FETCH_MENUS } from '../constants/actionType';

const initialState = [];

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_MENUS:
            return payload;

        default:
            return state
    }
}
