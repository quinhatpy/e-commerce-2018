import Cookies from 'js-cookie';
import { LOGOUT, SET_INFO_RESET_PASSWORD, SET_USER_CURRENT } from '../constants/actionType';

const initialState = {
    isAuthenticated: false,
    user: {},
    infoResetPassword: {}
};
export default (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_USER_CURRENT: {
            let updateState = { ...state };
            updateState.user = payload;
            updateState.isAuthenticated = true;
            return updateState;
        }

        case LOGOUT: {
            Cookies.remove('accessToken');
            return {
                isAuthenticated: false,
                user: {}
            }
        }

        case SET_INFO_RESET_PASSWORD: {
            let updateState = { ...state };
            updateState.infoResetPassword = payload;

            return updateState;
        }

        default:
            return state
    }
}
