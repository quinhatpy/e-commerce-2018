import { FETCH_BANNERS } from '../constants/actionType';

const initialState = {
    home: []
};

export default (state = initialState, { type, payload, position }) => {
    switch (type) {

        case FETCH_BANNERS:
            const cloneState = { ...state };
            cloneState[position] = payload;
            return cloneState;

        default:
            return state
    }
}
