import { FETCH_BRANDS_IN_HOME } from '../constants/actionType';

const initialState = {
    brandInHome: []
};

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_BRANDS_IN_HOME:
            const cloneState = { ...state };
            cloneState.brandInHome = payload;
            return cloneState;

        default:
            return state
    }
}
