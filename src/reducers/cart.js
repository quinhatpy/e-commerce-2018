import { ADD_TO_CART, INIT_CART, REMOVE_CART_ITEM, UPDATE_CART_ITEM } from '../constants/actionType';

const initialState = [];

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case INIT_CART: {
            return payload;
        }

        case ADD_TO_CART: {
            let stateClone = [...state];
            const cartItemIndex = findCartItemIndex(state, payload.id);

            if (cartItemIndex !== -1) {
                stateClone[cartItemIndex].quantity += payload.quantity;
            }
            else stateClone = [...stateClone, payload];
            localStorage.setItem('cart', JSON.stringify(stateClone));

            return stateClone;
        }

        case UPDATE_CART_ITEM: {
            let stateClone = [...state];
            stateClone[payload.index].quantity = payload.quantity;
            localStorage.setItem('cart', JSON.stringify(stateClone));

            return stateClone;
        }

        case REMOVE_CART_ITEM: {
            console.log(payload);
            const newState = state.filter((item, index) => index !== payload);
            localStorage.setItem('cart', JSON.stringify(newState));

            return newState;
        }

        default:
            return state
    }
}


const findCartItemIndex = (cart, productId) => {
    return cart.findIndex(item => item.id === productId)
}