import { connectRouter } from 'connected-react-router';
import { loadingBarReducer } from 'react-redux-loading-bar';
import { combineReducers } from 'redux';
import alert from './alert';
import article from './article';
import auth from './auth';
import banner from './banner';
import brand from './brand';
import breadcrumb from './breadcrumb';
import cart from './cart';
import category from './category';
import categoryBanner from './categoryBanner';
import menu from './menu';
import order from './order';
import product from './product';


export default (history) => combineReducers({
  router: connectRouter(history),
  loadingBar: loadingBarReducer,
  menu,
  category,
  banner,
  product,
  categoryBanner,
  brand,
  breadcrumb,
  cart,
  article,
  auth,
  alert,
  order
})