import { FETCH_BEST_SELLING, FETCH_HOT_PRODUCTS, FETCH_PRODUCT, FETCH_PRODUCTS, FETCH_PRODUCT_BOXES, FETCH_SIMILAR_PRODUCTS, FETCH_SUPER_DEALS } from '../constants/actionType';

const initialState = {
    superDeals: [],
    hotProducts: [],
    productBoxes: {},
    bestSelling: [],
    totalRows: 0,
    list: [],
    similar: [],
    item: {}
};

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_SUPER_DEALS: {
            const cloneState = { ...state };
            cloneState.superDeals = payload;
            return cloneState;
        }

        case FETCH_HOT_PRODUCTS: {
            const cloneState = { ...state };
            cloneState.hotProducts = payload;
            return cloneState;
        }

        case FETCH_PRODUCT_BOXES: {
            const cloneState = { ...state };
            cloneState.productBoxes = payload;
            return cloneState;
        }
        case FETCH_BEST_SELLING: {
            const cloneState = { ...state };
            cloneState.bestSelling = payload;
            return cloneState;
        }

        case FETCH_PRODUCTS: {
            const stateClone = { ...state };
            stateClone.list = payload.products;
            stateClone.totalRows = payload.totalRows;
            return stateClone;
        }

        case FETCH_SIMILAR_PRODUCTS: {
            const cloneState = { ...state };
            cloneState.similar = payload;
            return cloneState;
        }

        case FETCH_PRODUCT: {
            const cloneState = { ...state };
            cloneState.item = payload;
            return cloneState;
        }

        default:
            return state
    }
}
