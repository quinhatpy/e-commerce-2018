import { FETCH_CATEGORIES, FETCH_CATEGORY } from '../constants/actionType';

const initialState = {
    list: [],
    item: {}
};

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_CATEGORIES: {
            const cloneState = { ...state };
            cloneState.list = payload;
            return cloneState;
        }


        case FETCH_CATEGORY: {
            const cloneState = { ...state };
            cloneState.item = payload;
            return cloneState;
        }

        default:
            return state
    }
}
