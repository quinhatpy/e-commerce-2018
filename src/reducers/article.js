import { FETCH_ARTICLE, FETCH_ARTICLES } from '../constants/actionType';

const initialState = {
    totalRows: 0,
    list: [],
    item: {}
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case FETCH_ARTICLES: {
            const stateClone = { ...state };
            stateClone.list = payload.articles;
            stateClone.totalRows = payload.totalRows;
            return stateClone;
        }

        case FETCH_ARTICLE: {
            const cloneState = { ...state };
            cloneState.item = payload;
            return cloneState;
        }

        default:
            return state
    }
}
