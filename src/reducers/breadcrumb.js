import { FETCH_BREADCRUMB } from '../constants/actionType';

const initialState = [];

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case FETCH_BREADCRUMB: {
            return payload;
        }

        default:
            return state
    }
}
